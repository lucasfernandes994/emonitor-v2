package com.example.ferna_xj0mjha.emonitor.view.activity

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.view.adapter.ViewPagerAdapter
import com.example.ferna_xj0mjha.emonitor.view.fragment.AnaliseConsumoFragment
import com.example.ferna_xj0mjha.emonitor.view.fragment.AnalisePeriodoFragment


class AnaliseActivity : AppCompatActivity() {
    private var toolbar: Toolbar? = null
    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    private var pagerAdapter: ViewPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_analise)
        initComponents()
        initToolbar()
        setSetupViewPager()
    }

    private fun initComponents() {
        toolbar = findViewById<Toolbar>(R.id.toolbar)
        viewPager = findViewById<ViewPager>(R.id.viewpager)
        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        tabLayout!!.setupWithViewPager(viewPager)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            16908332 -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar() {
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp)
        setSupportActionBar(toolbar)
    }

    private fun setSetupViewPager() {
        pagerAdapter = ViewPagerAdapter(supportFragmentManager)
        pagerAdapter!!.addFragment(AnalisePeriodoFragment(), "Período")
        pagerAdapter!!.addFragment(AnaliseConsumoFragment(), "Consumo")
        //pagerAdapter!!.addFragment(AnaliseInfoFragment(), "Info")
        viewPager?.adapter = pagerAdapter
    }

    fun setCurrentItem(item: Int, smoothScroll: Boolean) {
        viewPager?.setCurrentItem(item, smoothScroll)
        val fragment = viewPager?.adapter?.instantiateItem(viewPager, viewPager!!.currentItem) as AnaliseConsumoFragment
        fragment.makeAnalysis()
    }
}
