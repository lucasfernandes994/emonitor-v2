package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import io.realm.RealmResults

/**
 * Created by lucas on 08/07/17.
 */

interface IModulo {

    interface IPresenter {
        fun setView(view: IView)
        fun getContext() : Context

        fun findModuloInDatabase()

        fun showProgress()
        fun dimissProgress()
        fun notifyAdapterDataChange()
    }

    interface IModel {
        fun findModuloInDatabase()
    }

    interface IView {
        fun exibirMensagemDeCadastroDeNovoDispositivo()
        fun showProgress()
        fun dimissProgress()
        fun setAdapter(modulos: RealmResults<Modulo>)
        fun notifyAdapterDataChange()

        companion object {
            val KEY = "modulo"
        }
    }
}
