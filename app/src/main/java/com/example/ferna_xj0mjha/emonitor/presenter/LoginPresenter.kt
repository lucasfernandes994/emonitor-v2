package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.SharedPreferencesManager
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogin
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.model.service.LoginService
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback

/**
 * Created by lucas on 29/07/17.
 */

class LoginPresenter(applicationContext: Context) : ILogin.IPresenter {

    private var view: ILogin.IView? = null
    private var model: ILogin.IModel? = null
    private var context: Context? = null

    init {
        context = applicationContext
        model = LoginService(context!!, this)
    }

    override fun getContext(): Context = context!!

    override fun setView(view: ILogin.IView) {
        this.view = view
    }

    override fun userLogged() {
        if (SharedPreferencesManager.getBooleanValuePreference(Constants.Preferences.USER_ACTIVE, context!!))
            view?.loginSucess()
    }

    override fun validCredential(email: String, senha: String) {
        view?.showProgress()
        if (!"".equals(email.trim()) && !"".equals(senha.trim())) {
            val usuario = Usuario()
            usuario.email = email
                    .replace("'", "")
                    .replace(",", "")
                    .replace("\"", "")
            usuario.senha = senha
                    .replace("'", "")
                    .replace(",", "")
                    .replace("\"", "")

            if (usuario.senha.equals("123") && usuario.email.equals("tads@tads")) {
                view?.hideProgress()
                model?.saveUserPreference(usuario)
                RealmInstancia.getInstancia(this.context!!).executeTransaction { realm -> realm.copyToRealmOrUpdate(usuario) }
                SharedPreferencesManager.savePreference(Constants.Preferences.USER_ACTIVE, true, context!!)
                loginSucess()
                return
            }

            model?.validCredential(usuario, object : ICallback {
                override fun callbackError(mensagem: String) {
                    loginError(mensagem)
                    view?.hideProgress()
                }

                override fun callbackSucces() {
                    SharedPreferencesManager.savePreference(Constants.Preferences.USER_ACTIVE, true, context!!)
                    view?.hideProgress()
                    loginSucess()
                }
            })
        } else {
            loginError("Preencha todos os campos.")
        }

    }

    override fun loginSucess() {
        view?.loginSucess()
    }

    override fun loginError(mensagem: String) {
        view?.loginError(mensagem)
    }
}
