package com.example.ferna_xj0mjha.emonitor.view.fragment

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.view.ViewCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.OvershootInterpolator
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumo
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.presenter.AnalisePeriodoPresenter
import com.example.ferna_xj0mjha.emonitor.view.activity.AnaliseActivity
import com.example.ferna_xj0mjha.emonitor.view.adapter.ModuloAnaliseAdapter
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import io.realm.OrderedRealmCollection
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by lucas on 23/09/2017.
 */
class AnalisePeriodoFragment : BaseFragment(), DatePickerDialog.OnDateSetListener, DialogInterface.OnCancelListener, IConsumo.IView {
    private var fabAnalisar: FloatingActionButton? = null
    private var rcModulo: RecyclerView? = null
    private var year: Int? = 0
    private var month: Int? = 0
    private var day: Int? = 0
    private var tvDataInicio: TextView? = null
    private var tvDataFim: TextView? = null
    private var FLAG_DATE: Boolean = false
    private var FLAG_ROTATE_FAB: Boolean = false
    private var consumo: Consumo? = null
    private var presenter: IConsumo.IPresenter? = null
    private var fabAddAnalise: FloatingActionButton? = null

    private var interpolator = OvershootInterpolator()

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_analise_periodo, container, false)
        initComponents(view!!)
        initPresenter()
        initHandlers()
        return view
    }

    override fun onStart() {
        super.onStart()
        presenter?.selectLast()

    }

    private fun initComponents(v: View) {
        fabAnalisar = v.findViewById(R.id.fabAnalisar)
        rcModulo = v.findViewById(R.id.rcModulo)
        tvDataInicio = v.findViewById(R.id.tvDataInicio)
        tvDataFim = v.findViewById(R.id.tvDataFim)
        fabAddAnalise = v.findViewById(R.id.fabAddAnalise)
    }

    private fun initHandlers() {
        fabAnalisar?.setOnClickListener {
            val format = SimpleDateFormat("dd/MM/yyyy")
            try {
                val dataInicio: Date = format.parse(tvDataInicio?.text as String?)
                val dataFim: Date = format.parse(tvDataFim?.text as String?)
                presenter?.genereteMultipleReport(dataInicio, dataFim, 100.0)
            } catch (e: ParseException) {
                Toast.makeText(context, "Data inválida!", Toast.LENGTH_SHORT).show()
            }
        }
        fabAddAnalise?.setOnClickListener {
            if (!FLAG_ROTATE_FAB)
                initAnalise()
            else if (consumo != null)
                presenter?.deleteConsumo(consumo!!)

        }
        tvDataInicio?.setOnClickListener {
            FLAG_DATE = true
            selectDate()
        }
        tvDataFim?.setOnClickListener {
            FLAG_DATE = false
            selectDate()
        }
    }

    private fun initAnalise() {
        this.presenter?.createNew()
    }

    private fun initRecyclerView(consumo: Consumo) {
        val modulos: OrderedRealmCollection<Modulo> = RealmInstancia.getInstancia(context).where(Modulo::class.java).findAll()!!
        val adapter = ModuloAnaliseAdapter(modulos, true, context, consumo)
        rcModulo?.adapter = adapter
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rcModulo?.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        rcModulo?.layoutManager = layoutManager
    }

    @SuppressLint("ResourceAsColor")
    private fun selectDate() {
        initDateTimeData()
        val cDefault: Calendar = Calendar.getInstance()
        cDefault.set(year!!, month!!, day!!)

        val datePickDialog = DatePickerDialog.newInstance(
                this,
                cDefault.get(Calendar.YEAR),
                cDefault.get(Calendar.MONTH),
                cDefault.get(Calendar.DAY_OF_MONTH)
        )

        datePickDialog?.setOnCancelListener { this@AnalisePeriodoFragment }
        datePickDialog?.show(activity.fragmentManager, "DatePickDialog")
        datePickDialog?.accentColor = R.color.colorPrimary
        datePickDialog?.isThemeDark = false
    }

    private fun initDateTimeData() {
        if (year == 0) {
            val c = Calendar.getInstance()
            year = c.get(Calendar.YEAR)
            month = c.get(Calendar.MONTH)
            day = c.get(Calendar.DAY_OF_MONTH)
        }
    }

    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {
        val tDefault = Calendar.getInstance()
        tDefault.set(year, monthOfYear, dayOfMonth)
        val format = SimpleDateFormat("dd/MM/yyyy")
        if (FLAG_DATE) {
            tvDataInicio?.text = format.format(tDefault.time)
        } else {
            tvDataFim?.text = format.format(tDefault.time)
        }
    }

    override fun onCancel(p0: DialogInterface?) {
        year = 0
        month = 0
        day = 0
    }

    override fun showReport() {
        (activity as AnaliseActivity).setCurrentItem(1, true)
    }

    override fun showReportMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    private fun initPresenter() {
        presenter = AnalisePeriodoPresenter(context)
        presenter?.setView(this)
    }

    override fun setConsumo(consumo: Consumo) {
        this.consumo = consumo
        ViewCompat.animate(fabAddAnalise).rotation(135f).withLayer().setDuration(300).setInterpolator(interpolator).start()
        FLAG_ROTATE_FAB = true
        initRecyclerView(consumo)
        rcModulo?.visibility = View.VISIBLE
        fabAnalisar?.visibility = View.VISIBLE
    }

    override fun susccesDelete() {
        ViewCompat.animate(fabAddAnalise).rotation(0f).withLayer().setDuration(300).setInterpolator(interpolator).start()
        FLAG_ROTATE_FAB = false
        rcModulo?.visibility = View.GONE
        fabAnalisar?.visibility = View.GONE
    }

    override fun errorDelete(mensagem: String) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show()
    }

}