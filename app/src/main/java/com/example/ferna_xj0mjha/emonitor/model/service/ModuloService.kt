package com.example.ferna_xj0mjha.emonitor.model.service

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.AsyncTask
import android.provider.SyncStateContract
import android.text.format.Formatter
import android.util.Log
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.rest.Mensagem
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloService
import com.example.ferna_xj0mjha.emonitor.model.repository.ModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackModulo
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackStatusModulo
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.apache.http.client.ClientProtocolException
import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.DefaultHttpClient
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.InetAddress
import java.net.URI
import java.net.URISyntaxException
import java.util.*

/**
 * Created by Lucas on 12/10/2017.
 */
class ModuloService : IModuloService.IModel {
    private var presenter: IModuloService.IPresenter? = null

    constructor(presenter: IModuloService.IPresenter) {
        this.presenter = presenter
    }

    constructor()

    init {
        this.presenter = presenter
    }

    @SuppressLint("StaticFieldLeak")
    override fun turnOn(modulo: Modulo, callback: ICallback) {
        val param = Constants.ParametroModulo.TURN_ON
        val port = modulo.porta
        val ip = modulo.ip
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String = sendCommand(param, ip!!, port!!, param)
            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                try {
                    val mensagem: Mensagem? = Gson().fromJson(result, Mensagem::class.java)
                    if (mensagem?.mensagem != null && mensagem.mensagem?.equals("sucess")!!)
                        callback.callbackSucces()
                    else
                        callback.callbackError(result!!)
                } catch (e: JsonSyntaxException) {
                    callback.callbackError(result!!)
                }
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    override fun turnOff(modulo: Modulo, callback: ICallback) {
        val param = Constants.ParametroModulo.TURN_OFF
        val port = modulo.porta
        val ip = modulo.ip
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String = sendCommand(param, ip!!, port!!, param)
            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                try {
                    val mensagem: Mensagem? = Gson().fromJson(result, Mensagem::class.java)
                    if (mensagem?.mensagem != null && mensagem.mensagem?.equals("sucess")!!)
                        callback.callbackSucces()
                    else
                        callback.callbackError(result!!)
                } catch (e: JsonSyntaxException) {
                    callback.callbackError(result!!)
                }
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    override fun findStatus(modulo: Modulo, callback: ICallbackStatusModulo) {
        val param = Constants.ParametroModulo.RELAY_STATUS
        val port = modulo.porta
        val ip = modulo.ip

        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String = sendCommand(param, ip!!, port!!, param)
            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                try {
                    val mensagem: Mensagem? = Gson().fromJson(result, Mensagem::class.java)
                    if (mensagem?.mensagem != null)
                        callback.callbackSucces(mensagem.mensagem!!)
                    else
                        callback.callbackError(result!!)
                } catch (e: JsonSyntaxException) {
                    callback.callbackError(result!!)
                }
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    override fun findModuloData(modulo: Modulo, callback: ICallbackModulo) {
        val param = Constants.ParametroModulo.GET_MODULO
        val port = modulo.porta
        val ip = modulo.ip
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String = sendCommand(param, ip!!, port!!, param)
            override fun onPostExecute(result: String?) {
                super.onPostExecute(result)
                try {
                    val modulo = Gson().fromJson(JSONObject(result).getJSONObject("mensagem").getJSONObject("modulo").toString(), Modulo::class.java)
                    if (modulo != null)
                        callback.callbackSucces(modulo)
                    else
                        callback.callbackError(result!!)
                } catch (e: JsonSyntaxException) {
                    callback.callbackError(result!!)
                }
            }
        }.execute()
    }

    override fun ping(modulo: Modulo, callback: ICallback): Boolean = ModuloPing().execute(modulo).get()

    override fun searchModulo(context: Context, ipRange: Array<String>) {

    }

    override fun sendCommand(parameterValue: String, ip: String, portNumber: String, parameterName: String): String {
        var serverResponse: String?
        try {
            val httpclient = DefaultHttpClient() // create an HTTP client
            // define the URL e.g. http://myIpaddress:myport/?pin=13 (to toggle pin 13 for example)
            val website = URI("http://$ip:$portNumber/$parameterValue")
            val getRequest = HttpGet() // create an HTTP GET object
            getRequest.uri = website // set the URL of the GET request
            val response = httpclient.execute(getRequest) // execute the request
            // get the ip address server's reply
            val content: InputStream?
            content = response.entity.content
            val `in` = BufferedReader(InputStreamReader(
                    content!!
            ))
            serverResponse = `in`.readLine()
            // Close the connection
            content.close()
        } catch (e: ClientProtocolException) {
            // HTTP error
            serverResponse = e.message
            e.printStackTrace()
        } catch (e: IOException) {
            // IO error
            serverResponse = e.message
            e.printStackTrace()
        } catch (e: URISyntaxException) {
            // URL syntax error
            serverResponse = e.message
            e.printStackTrace()
        } catch (e: java.lang.IllegalArgumentException) {
            serverResponse = e.message
            e.printStackTrace()
        }
        return serverResponse!!
    }

    override fun getIpRange(): ArrayList<String>? {
        val modulos = presenter?.getContext()?.let { ModuloRepository.findAll(it) }
        val ips: ArrayList<String>? = null
        modulos?.forEach { m ->
            ips?.add(m.ip!!)
        }
        return ips
    }

    override fun findTime(modulo: Modulo, callback: ICallback) {
        try {
            val value = sendCommand(Constants.ParametroModulo.TIME, modulo.ip!!, modulo.porta!!, Constants.ParametroModulo.TIME)
            modulo.dispositivo?.time = value.toLong()
            ModuloRepository.saveOrUpdate(modulo)
            callback.callbackSucces()
        } catch (e: Exception) {
            callback.callbackError("Não foi possível conectar.")
        }
    }

    override fun findWatts(modulo: Modulo, callback: ICallback) {
        try {
            val value = sendCommand(Constants.ParametroModulo.WATTS, modulo.ip!!, modulo.porta!!, Constants.ParametroModulo.WATTS)
            modulo.dispositivo?.watts = value.toDouble()
            ModuloRepository.saveOrUpdate(modulo)
            callback.callbackSucces()
        } catch (e: Exception) {
            callback.callbackError("Não foi possível conectar.")
        }
    }

    inner class ModuloConnect(private val context: Context?, private val presenter: IModulo.IPresenter, private val ipRange: ArrayList<String>) : AsyncTask<ArrayList<Modulo>, Void, ArrayList<Modulo>>() {
        var modulos = ArrayList<Modulo>()
        override fun onPreExecute() {
            super.onPreExecute()
            //consumoPresenter.showProgress();
        }

        override fun doInBackground(vararg params: ArrayList<Modulo>): ArrayList<Modulo> {
            try {
                if (context != null) {
                    val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                    val activeNetwork = cm.activeNetworkInfo
                    val wm = context.getSystemService(Context.WIFI_SERVICE) as WifiManager

                    val connectionInfo = wm.connectionInfo
                    val ipAddress = connectionInfo.ipAddress
                    val ipString = Formatter.formatIpAddress(ipAddress)
                    val prefix = ipString.substring(0, ipString.lastIndexOf(".") + 1)

                    for (i in ipRange.indices) {
                        val testIp = prefix + ipRange[i]
                        val address = InetAddress.getByName(testIp)
                        val reachable = address.isReachable(111)
                        val hostName = address.hostName
                        val macAdress = address.hostAddress
                        val classe = address.javaClass.toString()
                        val redeNome = activeNetwork.extraInfo
                        if (reachable) {
                            val modulo = Modulo()
                            modulo.nome = hostName.toString()
                            modulo.ip = testIp.toString()
                            modulo.macAdress = macAdress.toString()
                            modulo.classe = classe
                            modulos.add(modulo)
                        }
                    }
                }
            } catch (t: Throwable) {
                Log.e(TAG, "Well that's not good.", t)
                presenter.dimissProgress()
            }

            return modulos
        }

        override fun onPostExecute(modulos: ArrayList<Modulo>) {
            super.onPostExecute(modulos)
            //consumoPresenter.dimissProgress();
        }

        private val TAG = SyncStateContract.Constants.ACCOUNT_TYPE + "nstask"
    }

    inner class ModuloPing : AsyncTask<Modulo, Void, Boolean>() {
        override fun doInBackground(vararg p0: Modulo?): Boolean {
            val address = InetAddress.getByName(p0[0]?.ip)
            return address.isReachable(111)
        }
    }

}