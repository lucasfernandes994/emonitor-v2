package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.interfaces.INetwork
import com.example.ferna_xj0mjha.emonitor.model.service.Network

/**
 * Created by ferna_xj0mjha on 12/08/2017.
 */
class NetworkPresenter(context: Context) : INetwork.IPresenter {
    private var context: Context ? =null
    private var view: INetwork.IView? = null
    private var model: INetwork.IModel? = null

    init {
        this.context = context
        model = Network(this)
    }

    override fun setView(view: INetwork.IView) {
        this.view = view
    }

    override fun getContext(): Context = context!!

    override fun isNetworkAvailable(context: Context) {
        model?.isNetworkAvailable(getContext())
    }

    override fun networkError() {
        view?.networkError()
    }

    override fun noNetwork() {
        view?.noNetwork()
    }
}