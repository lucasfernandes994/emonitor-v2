package com.example.ferna_xj0mjha.emonitor.view.activity

import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import br.com.sapereaude.maskedEditText.MaskedEditText
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ITarifa
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.presenter.ModuloRepositoryPresenter
import com.example.ferna_xj0mjha.emonitor.presenter.TarifaPresenter


class CadastroDispositivoActivity : AppCompatActivity(), IModuloRepository.IView, ITarifa.IView {
    private var toolbar: Toolbar? = null
    private var et_nome: EditText? = null
    private var et_descricao: EditText? = null
    private var et_ip: MaskedEditText? = null
    private var presenter: IModuloRepository.IPresenter? = null
    private var bundle: Bundle? = null
    private var modulo: Modulo? = null
    private var etValorTarifa: EditText? = null
    private var fabAddTarifa: FloatingActionButton? = null
    private var tarifaPresenter: ITarifa.IPresenter? = null

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cadastro_dispositivo)
        initComponents()
        initBundle()
        initHandlers()
        initTarifa()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater? = menuInflater
        inflater?.inflate(R.menu.menu_cad_dispositivo, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            R.id.save_dispositivo -> {
                if (etValorTarifa?.text?.isEmpty()!!) {
                    Toast.makeText(applicationContext, getString(R.string.cadastre_valor_tarifa), Toast.LENGTH_SHORT).show()
                    return false
                }
                if (bundle == null) {
                    val modulo = Modulo()
                    val dispositivo = Dispositivo()
                    dispositivo.nome = et_nome?.text.toString()
                    dispositivo.descricao = et_descricao?.text.toString()
                    modulo.dispositivo = dispositivo
                    modulo.ip = et_ip?.text.toString()
                    modulo.porta = "80"
                    modulo.let { presenter?.saveModulo(it) }
                } else {
                    val modulo = Modulo()
                    val dispositivo = Dispositivo()
                    modulo.id = this.modulo?.id
                    dispositivo.nome = et_nome?.text.toString()
                    dispositivo.descricao = et_descricao?.text.toString()
                    modulo.dispositivo = dispositivo
                    modulo.ip = et_ip?.text.toString()
                    modulo.porta = "80"
                    presenter?.upadateModulo(modulo)
                }
                val intent = Intent(Constants.BroadcastReceiver.ANALISYS_RECEIVER)
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent)
            }
            16908332 -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("ResourceAsColor")
    private fun initComponents() {
        toolbar = findViewById(R.id.toolbar2)
        toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp)
        setSupportActionBar(toolbar)
        toolbar?.title = "EMonitor"

        et_nome = findViewById(R.id.et_nome)
        et_descricao = findViewById(R.id.et_descricao)
        et_ip = findViewById(R.id.et_ip)
        etValorTarifa = findViewById(R.id.et_valor_tarifa)
        fabAddTarifa = findViewById(R.id.fabAddTarifa)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

    init {
        presenter = ModuloRepositoryPresenter(this)
        presenter?.setView(this)
    }

    fun initBundle() {
        this.bundle = intent.extras
        if (bundle != null) {
            modulo = RealmInstancia.getInstancia(applicationContext)
                    .where(Modulo::class.java)
                    .equalTo("id", bundle?.get(Constants.IntentParam.ID_MODULO) as Int)
                    .findFirst()
            et_nome?.setText(modulo?.dispositivo?.nome)
            et_descricao?.setText(modulo?.dispositivo?.descricao)
            et_ip?.setText(modulo?.ip?.replace(".", ""))
        }
    }

    fun initTarifa() {
        tarifaPresenter = TarifaPresenter(applicationContext)
        tarifaPresenter?.setView(this)
        tarifaPresenter?.selectLast()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun initHandlers() {
        fabAddTarifa?.setOnClickListener({
            val txtTarifa = EditText(this)
            txtTarifa.setHint("Valor tarifa")
            txtTarifa.setBackgroundDrawable(resources.getDrawable(R.drawable.retangular_ripple_blue))
            txtTarifa.inputType = InputType.TYPE_NUMBER_FLAG_SIGNED

            val builder = AlertDialog.Builder(this)
                    .setTitle("Tarifa de energia elétrica")
                    .setMessage("Insira o valor da tarifa elétrica que consta em sua conta de luz.")
                    .setView(txtTarifa)
                    .setPositiveButton("Salvar", { dialog, whichButton ->
                        tarifaPresenter?.salvarTarifa(txtTarifa.getText().toString())
                    })
                    .setNegativeButton("Cancelar", { dialog, whichButton ->
                    })
                    .create()

            builder.show()
            val nbutton = builder.getButton(DialogInterface.BUTTON_NEGATIVE)
            nbutton.setBackgroundColor(Color.WHITE)
            nbutton.setTextColor(resources.getColor(R.color.colorPrimary))
            val pbutton = builder.getButton(DialogInterface.BUTTON_POSITIVE)
            pbutton.setBackgroundColor(Color.WHITE)
            pbutton.setTextColor(resources.getColor(R.color.colorPrimary))
            val pneutral = builder.getButton(DialogInterface.BUTTON_NEUTRAL)
            pneutral.setBackgroundColor(Color.WHITE)
            pneutral.setTextColor(resources.getColor(R.color.colorPrimary))
        })
    }

    override fun sucessSave() {
        Toast.makeText(applicationContext, "Dispositivo cadastrado com sucesso.", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun errorDelete() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun sucessDelete() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun errorCreateUpdate() {
        Toast.makeText(applicationContext, "Não foi possível salvar.", Toast.LENGTH_SHORT).show()
    }

    override fun errorSameIp() {
        Toast.makeText(applicationContext, "Este ip já foi utilizado por outro dispositivo.", Toast.LENGTH_SHORT).show()
    }

    override fun sucessUpadate() {
        Toast.makeText(applicationContext, "Dispositivo atualizado com sucesso.", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun succesSave() {
        Toast.makeText(applicationContext, "Valor da tarifa salvo.", Toast.LENGTH_SHORT).show()
    }

    override fun errorSave(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun setTarifa(valor: Double) {
        etValorTarifa?.setText(valor.toString())
    }
}
