package com.example.ferna_xj0mjha.emonitor.model.repository

import android.annotation.SuppressLint
import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.abstracts.AbsAnalise
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloRepository
import io.realm.RealmResults
import java.util.*

class ModuloRepository : IModuloRepository.IModel, AbsAnalise {
    var context: Context? = null
    var presenter: IModuloRepository.IPresenter? = null

    override fun deleteAllFromRealm(context: Context) {
        RealmInstancia.getInstancia(context).where(Modulo::class.java).findAll().deleteAllFromRealm()
    }

    constructor(context: Context, presenter: IModuloRepository.IPresenter) {
        this.presenter = presenter
        this.context = context
    }

    constructor()

    override fun findAllModulo(context: Context): RealmResults<Modulo> = RealmInstancia.getInstancia(context).where(Modulo::class.java).findAll()

    override fun update(m: Modulo, context: Context) {
        try {
            RealmInstancia.getInstancia(context).executeTransaction { realm ->
                realm.copyToRealmOrUpdate(m)
                presenter?.sucessUpadate()
            }
        } catch (e: Exception) {
            presenter?.errorCreateUpdate()
        }
    }

    override fun findAll(context: Context): RealmResults<Modulo> = RealmInstancia.getInstancia(context).where(Modulo::class.java).findAll()

    override fun save(m: Modulo, context: Context) {
        try {
            RealmInstancia.getInstancia(context).executeTransaction { realm ->
                m.id = nextId(context)
                m.dispositivo?.id = DispositivoRepository.nextId(context)
                realm.copyToRealmOrUpdate(m)
                presenter?.sucessSave()
            }
        } catch (e: Exception) {
            presenter?.errorCreateUpdate()
        }
    }


    override fun delete(modlulo: Modulo, context: Context) {
        try {
            RealmInstancia.getInstancia(context).executeTransaction { realm ->
                realm.where(Modulo::class.java).equalTo("id", modlulo.id).findFirst().deleteFromRealm()
                presenter?.sucessDelete()
            }
        } catch (e: Exception) {
            presenter?.errorDelete()
        }
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        private var context: Context? = null

        init {
            this.context = context
        }

        fun saveOrUpdate(modulo: ArrayList<Modulo>) {
            RealmInstancia.getInstancia(this.context!!).executeTransaction { realm ->
                modulo.forEach { m ->
                    val modulo: Modulo = realm.where(Modulo::class.java).equalTo("macAdress", m.macAdress).findFirst()
                    modulo.nome = m.nome
                    modulo.ip = m.ip
                    modulo.classe = m.classe
                    modulo.porta = m.porta
                    modulo.status = m.status
                    modulo.dispositivo = m.dispositivo
                    RealmInstancia.getInstancia(context!!).copyToRealmOrUpdate(modulo)
                }
            }
        }

        fun saveOrUpdate(m: Modulo) {
            RealmInstancia.getInstancia(context!!).executeTransaction { realm ->
                val modulo: Modulo = realm.where(Modulo::class.java).equalTo("macAdress", m.macAdress).findFirst()
                modulo.nome = m.nome
                modulo.ip = m.ip
                modulo.classe = m.classe
                modulo.porta = m.porta
                modulo.status = m.status
                modulo.dispositivo = m.dispositivo
                RealmInstancia.getInstancia(context!!).copyToRealmOrUpdate(modulo)
            }
        }

        fun findAll(context: Context): RealmResults<Modulo>? =
                RealmInstancia.getInstancia(context).where(Modulo::class.java).findAll()

        private fun nextId(context: Context): Int {
            return try {
                RealmInstancia.getInstancia(context).where(Modulo::class.java).max("id").toInt() + 1
            } catch (e: ArrayIndexOutOfBoundsException) {
                0
            } catch (ex: NullPointerException) {
                0
            }
        }

        fun findById(context: Context, id: Int): Modulo {
            return RealmInstancia.getInstancia(context)
                    .where(Modulo::class.java)
                    .equalTo("id", id)
                    .findFirst()
        }

        fun saveModuloWithBackgroundRealm(moduloData: Modulo, context: Context, id: Int) {
            val backgroundThreadRealm = RealmInstancia.getInstancia(context)
            backgroundThreadRealm.executeTransaction { realm ->
                val moduloRealm = realm.where(Modulo::class.java).equalTo("id", id).findFirst()
                moduloRealm.macAdress = moduloData.macAdress
                moduloRealm.status = moduloData.status
                moduloRealm.porta = moduloData.porta
                moduloRealm.ip = moduloData.ip

                if (moduloData.sincronazed == 0) {
                    var totalTime: Long = 0
                    if (moduloRealm.dispositivo?.time == null)
                        totalTime = moduloData.dispositivo?.time as Long
                    else
                        totalTime = moduloRealm.dispositivo?.time as Long + moduloData.dispositivo?.time as Long
                    moduloRealm.dispositivo?.time = totalTime
                } else {
                    if (moduloRealm.dispositivo?.time != null)
                        moduloRealm.dispositivo?.time = moduloRealm.dispositivo?.time as Long + (moduloData.dispositivo?.time as Long) - (moduloRealm.dispositivo?.lastTimeSincronized as Long)
                    else
                        moduloRealm.dispositivo?.time = (moduloData.dispositivo?.time as Long)
                }
                moduloRealm.dispositivo?.lastTimeSincronized = moduloData.dispositivo?.time

                if (moduloRealm.dispositivo?.dataInicio == null) {
                    val calendar = Calendar.getInstance()
                    calendar.time = Date()
                    moduloRealm?.dispositivo?.time!!.toInt().let {
                        calendar.add(Calendar.MILLISECOND, it * -1)
                        moduloRealm.dispositivo?.dataInicio = calendar.time
                    }
                }
                moduloRealm.dispositivo?.dataFim = Date()
                if (moduloData.dispositivo?.watts != null)
                    moduloRealm.dispositivo?.watts = moduloData.dispositivo?.watts

                if (moduloData.dispositivo?.corrente != null)
                    moduloRealm.dispositivo?.corrente = moduloData.dispositivo?.corrente

                realm.copyToRealmOrUpdate(moduloRealm)

                moduloRealm?.dispositivo?.let { AnaliseRepository.salvarAnalise(realm, it) }
            }
        }
    }
}