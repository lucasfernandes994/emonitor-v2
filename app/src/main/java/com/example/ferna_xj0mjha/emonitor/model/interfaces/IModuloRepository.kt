package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import io.realm.RealmResults

/**
 * Created by ferna_xj0mjha on 19/08/2017.
 */
interface IModuloRepository {
    interface IPresenter {
        fun setView(view: IView)

        fun saveModulo(modlulo: Modulo)
        fun upadateModulo(modulo: Modulo)
        fun delete(modlulo: Modulo)
        fun findAll(context: Context): RealmResults<Modulo>
        fun findModuloById(id : Int, context: Context) : Modulo

        fun sucessSave()
        fun sucessUpadate()
        fun sucessDelete()
        fun errorDelete()
        fun errorCreateUpdate()
        fun errorSameIp()
    }

    interface IModel {
        fun save(m: Modulo, context: Context)
        fun update(m: Modulo, context: Context)
        fun delete(modlulo: Modulo, context: Context)
        fun findAll(context: Context): RealmResults<Modulo>
    }

    interface IView {
        fun sucessSave()
        fun sucessUpadate()
        fun errorCreateUpdate()
        fun errorDelete()
        fun sucessDelete()
        fun errorSameIp()
    }
}