package com.example.ferna_xj0mjha.emonitor.view.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

/**
 * Created by ferna_xj0mjha on 23/09/2017.
 */
class ViewPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm) {
    private var fragments = ArrayList<Fragment>()
    private var fragmentsTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment? = fragments[position]

    override fun getCount(): Int = fragments.size

    fun addFragment(fragment: Fragment, title: String) {
        fragments.add(fragment)
        fragmentsTitleList.add(title)
    }

    override fun getPageTitle(position: Int): CharSequence = fragmentsTitleList[position]
}