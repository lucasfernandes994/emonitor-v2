package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context

/**
 * Created by ferna_xj0mjha on 11/08/2017.
 */
interface INetwork {
    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context
        fun isNetworkAvailable(context: Context)
        fun networkError()
        fun noNetwork()
    }

    interface IModel {
        fun isNetworkAvailable(context: Context)
    }

    interface IView {
        fun networkError()
        fun noNetwork()
        fun showProgress()
        fun dimissProgress()

        companion object {
            val KEY = "network"
        }
    }
}