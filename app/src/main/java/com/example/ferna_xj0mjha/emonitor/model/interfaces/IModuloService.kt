package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackModulo
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackStatusModulo
import java.util.*

/**
 * Created by ferna_xj0mjha on 12/10/2017.
 */
interface IModuloService {

    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context

        fun findModuloData(modulo: Modulo, callback: ICallbackModulo)
        fun turnOff(modulo: Modulo)
        fun turnOn(modulo: Modulo)
        fun findStatus(modulo: Modulo)
        fun ping(modulo: Modulo)

        fun showMessage(mensagem: String)
        fun showModuloData()
        fun succesTurnOff()
        fun errorTurnOff(mensagem: String)
        fun succesTurnOn()
        fun errorTurnOn(mensagem: String)
        fun successPing()
        fun errorPing(mensagem: String)
        fun succesStatus(value: Boolean)
        fun errorStatus(mensagem: String)
        fun showProgress(mensagem: String, title: String)
        fun dismissProgress()
    }

    interface IModel {
        fun searchModulo(context: Context, ipRange: Array<String>)
        fun sendCommand(parametrerValue: String, ip: String, portNuber: String, parametrerName: String): String
        fun getIpRange(): ArrayList<String>?

        fun findModuloData(modulo: Modulo, callback: ICallbackModulo)
        fun findTime(modulo: Modulo, callback: ICallback)
        fun findWatts(modulo: Modulo, callback: ICallback)
        fun turnOff(modulo: Modulo, callback: ICallback)
        fun turnOn(modulo: Modulo, callback: ICallback)
        fun findStatus(modulo: Modulo, callback: ICallbackStatusModulo)
        fun ping(modulo: Modulo, callback: ICallback): Boolean
    }

    interface IView {
        fun showMessage(mensagem: String)
        fun showModuloData()
        fun succesTurnOff()
        fun errorTurnOff(mensagem: String)
        fun succesTurnOn()
        fun errorTurnOn(mensagem: String)
        fun successPing()
        fun errorPing(mensagem: String)
        fun succesStatus(value: Boolean)
        fun errorStatus(mensagem: String)
        fun showProgress(mensagem: String, title: String)
        fun dismissProgress()
    }
}