package com.example.ferna_xj0mjha.emonitor.model.repository

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.abstracts.AbsConsumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IAnalise
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumoRepository
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ferna_xj0mjha on 27/09/2017.
 */
class ConsumoRepository : AbsConsumo, IConsumo.IModel, IAnalise.IModel {
    var consumoPresenter: IConsumo.IPresenter? = null
    var analisePresenter: IAnalise.IPresenter? = null

    constructor(presenter: IConsumo.IPresenter) {
        this.consumoPresenter = presenter
    }

    constructor(analisePresenter: IAnalise.IPresenter) {
        this.analisePresenter = analisePresenter
    }

    constructor()

    override fun deleteAllFromRealm(context: Context) {
        RealmInstancia.getInstancia(context).where(Consumo::class.java).findAll().deleteAllFromRealm()
    }

    override fun accertEqualNull(): Boolean {
        return try {
            RealmInstancia.getInstancia(analisePresenter?.getContext()!!).where(Consumo::class.java).findAll() == null
        } catch (e: ArrayIndexOutOfBoundsException) {
            false
        } catch (ex: NullPointerException) {
            false
        }
    }

    override fun findLastConsumo(): Consumo {
        return analisePresenter?.getContext()?.let {
            RealmInstancia.getInstancia(it).where(Consumo::class.java).findAll().last()
        }!!
    }

    override fun save(context: Context, consumo: Consumo) {
        RealmInstancia.getInstancia(context).executeTransaction { realm ->
            consumo.id = nextId(context)
            realm.copyToRealmOrUpdate(consumo)
        }
    }

    companion object {
        private fun nextId(context: Context): Int {
            return try {
                RealmInstancia.getInstancia(context).where(Consumo::class.java).max("id").toInt() + 1
            } catch (e: ArrayIndexOutOfBoundsException) {
                0
            } catch (ex: NullPointerException) {
                0
            }
        }
    }

    override fun genereteMultipleReport(consumo: Consumo, dataInicio: Date, dataFim: Date) {
        val format = SimpleDateFormat("dd/MM/yyyy")
        val dtInicio: Date = format.parse(format.format(dataInicio))
        val dtFim: Date = format.parse(format.format(dataFim))

        val analises = try {
            AnaliseRepository.getAnaliseByDataInicioEdataFim(dtFim, dtInicio, consumoPresenter?.getContext()!!)
        } catch (e: Exception) {
            null
        }
        if (analises != null) {
            RealmInstancia.getInstancia(consumoPresenter?.getContext()!!).executeTransaction { r ->
                consumo.dataInicio = dtInicio
                consumo.dataFim = dtFim
                r.copyToRealmOrUpdate(consumo)
            }
        }
        consumoPresenter?.showReport(analises != null && analises.size > 0)
    }

    override fun selectLast() {
        val consumo = RealmInstancia.getInstancia(consumoPresenter?.getContext()!!)
                .where(Consumo::class.java)
                .findAllSorted("id", Sort.DESCENDING).first(null)

        if (consumo != null) {
            consumoPresenter?.setConsumo(consumo)
        }

    }

    override fun deleteFromRealm(context: Context, consumo: Consumo, presenter: IConsumoRepository.IPresenter) =
            try {
                RealmInstancia.getInstancia(context).executeTransaction({ realm ->
                    realm.where(Consumo::class.java).equalTo("id", consumo.id).findFirst().deleteFromRealm()
                })
                presenter.sucessDelete()
            } catch (e: Exception) {
                presenter.errorDelete()
            }

    fun selectAllConsumoFromRealm(context: Context): RealmResults<Consumo> =
            RealmInstancia.getInstancia(context).where(Consumo::class.java).findAll()

    override fun deleteConsumo(consumo: Consumo) {
        try {
            RealmInstancia.getInstancia(consumoPresenter?.getContext()!!).executeTransaction(Realm.Transaction { realm ->
                realm.where(Consumo::class.java).equalTo("id", consumo.id).findFirst().deleteFromRealm()
            })
            consumoPresenter?.susccesDelete()
        } catch (e: Exception) {
            consumoPresenter?.errorDelete("Não foi possível deletar esta nova análise.")
        }

    }

    override fun createNew() {
        val consumo = Consumo()
        consumo.id = nextId(consumoPresenter?.getContext()!!)
        consumo.date = Date()
        save(consumoPresenter?.getContext()!!, consumo)
        consumoPresenter?.setConsumo(consumo)
    }

}