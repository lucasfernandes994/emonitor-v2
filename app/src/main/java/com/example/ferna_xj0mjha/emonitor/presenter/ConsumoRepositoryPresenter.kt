package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.abstracts.AbsConsumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumoRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.ConsumoRepository

/**
 * Created by ferna_xj0mjha on 07/10/2017.
 */
class ConsumoRepositoryPresenter(context: Context) : IConsumoRepository.IPresenter {
    private var view: IConsumoRepository.IView? = null
    private var context: Context? = null
    private var consumoRepository: AbsConsumo? = null

    init {
        this.context = context
    }

    override fun deleteConsumo(consumo: Consumo) {
        consumoRepository = ConsumoRepository()
        consumoRepository?.deleteFromRealm(context!!, consumo, this)
    }

    override fun sucessDelete() {
        view?.sucessDelete()
    }

    override fun errorDelete() {
        view?.errorDelete()
    }


    override fun setView(view: IConsumoRepository.IView) {
        this.view = view
    }

    override fun getContext(): Context = context!!
}