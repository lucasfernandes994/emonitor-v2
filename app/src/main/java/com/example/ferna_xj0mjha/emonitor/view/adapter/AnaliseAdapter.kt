package com.example.ferna_xj0mjha.emonitor.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Analise
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import java.text.DecimalFormat

/**
 * Created by lucas on 28/11/2017.
 */
open class AnaliseAdapter(data: OrderedRealmCollection<Dispositivo>?, autoUpdate: Boolean, consumo: Consumo, context: Context)
    : RealmRecyclerViewAdapter<Dispositivo, AnaliseAdapter.AnaliseViewHolder>(data, autoUpdate) {
    var consumo: Consumo? = null
    var context: Context? = null

    init {
        this.consumo = consumo
        this.context = context
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): AnaliseViewHolder =
            AnaliseAdapter.AnaliseViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_analise, parent, false))

    override fun onBindViewHolder(holder: AnaliseViewHolder?, position: Int) {
        holder?.nome?.text = getItem(position)?.nome
        val consumo = context?.let { RealmInstancia.getInstancia(it).where(Consumo::class.java).findAll().last() }
        
        consumo?.dispositivos?.forEach { d ->
            val analises = RealmInstancia.getInstancia(context!!)
                    .where(Analise::class.java).greaterThanOrEqualTo("date", consumo.dataInicio!!)
                    .lessThanOrEqualTo("date", consumo.dataFim!!).equalTo("dispositovo.id", d.id).findAll()
            if (analises != null && analises.size > 0) {
                holder?.nome?.text = getItem(position)?.nome
                var sum: Number = analises.sum("valor")
                sum = sum.toFloat() * 1000 * 60 * 60

                val df = DecimalFormat("###.0000000000")
                val results = "R$ 0" + df.format(sum).toString()
                holder?.valor?.text = results
            }
        }
    }

    class AnaliseViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var valor: TextView? = null
        var nome: TextView? = null

        init {
            valor = itemView?.findViewById(R.id.tv_valor)
            nome = itemView?.findViewById(R.id.tv_nome)
        }
    }
}