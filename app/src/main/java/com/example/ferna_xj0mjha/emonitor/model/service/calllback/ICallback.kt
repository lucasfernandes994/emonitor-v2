package com.example.ferna_xj0mjha.emonitor.model.service.calllback

/**
 * Created by ferna_xj0mjha on 05/08/2017.
 */
interface ICallback {
    fun callbackSucces()
    fun callbackError(mensagem: String)
}