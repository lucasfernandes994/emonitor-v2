package com.example.ferna_xj0mjha.emonitor.model.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by ferna_xj0mjha on 01/11/2017.
 */
open class Tarifa : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var valor: Double? = null
}