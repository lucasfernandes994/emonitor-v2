package com.example.ferna_xj0mjha.emonitor.view.activity

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

/**
 * Created by lucas on 08/07/17.
 */

open class BaseActivity : AppCompatActivity() {
    protected fun replaceFragment(container: Int, fragment: Fragment) {
        val manager = supportFragmentManager
        val transaction = manager.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        transaction.replace(container, fragment)
        transaction.commit()
    }
}
