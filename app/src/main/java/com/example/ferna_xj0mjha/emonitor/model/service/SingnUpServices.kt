package com.example.ferna_xj0mjha.emonitor.model.service

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.SharedPreferencesManager
import com.example.ferna_xj0mjha.emonitor.infra.rest.Mensagem
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ISingUp
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.google.gson.Gson
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import org.json.JSONObject

class SingnUpServices(presenter: ISingUp.IPresenter, context: Context) : ISingUp.IModel {
    private var presenter: ISingUp.IPresenter? = null
    private var context: Context

    override fun `createAccount`(usuario: Usuario, callback: ICallback) {
        val client = AsyncHttpClient()
        val jsonParams = JSONObject()
        jsonParams.put("nome", usuario.nome)
        jsonParams.put("email", usuario.email)
        jsonParams.put("senha", usuario.senha)
        val stringEntity = StringEntity(jsonParams.toString())
        client.post(context, Constants.Url.LOGIN_URL + "usuario", stringEntity, "application/json", object : JsonHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                if (statusCode == 200) {
                    try {
                        val mensagem: Mensagem = Gson().fromJson(response.toString(), Mensagem::class.java)
                        if(mensagem.mensagem.equals("sucess")){
                            saveUserPreference(usuario)
                            RealmInstancia.getInstancia(context).executeTransaction { realm -> realm.copyToRealm(usuario) }
                            callback.callbackSucces()
                        }else{
                            callback.callbackError("Parâmetros inválidos.")
                        }
                    } catch (e: Exception) {
                        callback.callbackError("Erro ao conectar.")
                    }
                }
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                callback.callbackError("Erro ao conectar.")
            }
        })
    }

    fun saveUserPreference(usuario: Usuario) {
        SharedPreferencesManager.savePreference(Constants.UserData.USER_DATABASE, usuario.email, context)
        SharedPreferencesManager.savePreference(Constants.Preferences.USER_ACTIVE, true, context)
    }

    init {
        this.presenter = presenter
        this.context = context
    }

}