package com.example.ferna_xj0mjha.emonitor.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by lucas on 26/07/17.
 */

open class Usuario : RealmObject() {
    @PrimaryKey
    @SerializedName("id")
    var id: String? = null

    @SerializedName("nome")
    @Expose
    var nome: String? = null

    @Expose
    @SerializedName("senha")
    var senha: String? = null

    @SerializedName("email")
    @Expose
    var email: String? = null
}
