package com.example.ferna_xj0mjha.emonitor.view.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v4.content.LocalBroadcastManager
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.repository.ModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.service.ModuloService
import com.example.ferna_xj0mjha.emonitor.view.fragment.AnaliseFragment
import com.example.ferna_xj0mjha.emonitor.view.fragment.HomeFragment
import com.example.ferna_xj0mjha.emonitor.view.fragment.InfoFragment
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.json.JSONException
import org.json.JSONObject


class MainActivity : BaseActivity() {
    private var navigation: BottomNavigationView? = null
    private var mainTaskStop = false

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                replaceFragment(R.id.content, HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                replaceFragment(R.id.content, AnaliseFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                replaceFragment(R.id.content, InfoFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigation = findViewById(R.id.navigation)
        navigation!!.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        initComponents()
    }

    override fun onStart() {
        super.onStart()
        mainTaskStop = true
        getetModeuloData()
    }

    override fun onResume() {
        super.onResume()
        mainTaskStop = true
    }

    override fun onPause() {
        super.onPause()
        mainTaskStop = false
    }

    override fun onDestroy() {
        super.onDestroy()
        mainTaskStop = false
    }

    private fun initComponents() {
        replaceFragment(R.id.content, HomeFragment())
    }


    private fun getetModeuloData() {
        if (mainTaskStop) {
            val modulos = ModuloRepository.findAll(applicationContext)
            Handler().postDelayed({
                modulos?.forEach { m ->
                    ModuloData(m, this@MainActivity).execute()
                }
                getetModeuloData()
            }, 1000)
        }
    }

    @SuppressLint("StaticFieldLeak")
    class ModuloData(modulo: Modulo, val context: Context) : AsyncTask<Void, Void, String>() {
        private val param = Constants.ParametroModulo.GET_MODULO
        private val port = modulo.porta
        private val ip = modulo.ip
        private val id = modulo.id

        override fun doInBackground(vararg p0: Void?): String =
                ModuloService().sendCommand(param, ip!!, port!!, param)

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            try {
                val moduloData = Gson().fromJson(JSONObject(result).getJSONObject("mensagem").getJSONObject("modulo").toString(), Modulo::class.java)
                moduloData.porta = port
                moduloData.ip = ip
                id?.let { ModuloRepository.saveModuloWithBackgroundRealm(moduloData, context, it) }
                val intent = Intent(Constants.BroadcastReceiver.ANALISYS_RECEIVER)
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            } catch (ex: java.lang.IllegalStateException) {
                ex.printStackTrace()
            } catch (ex: JSONException) {
                ex.printStackTrace()
            } catch (ex: JsonSyntaxException) {
                ex.printStackTrace()
            } finally {
                this.cancel(true)
            }
        }

    }
}
