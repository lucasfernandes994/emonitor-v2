package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.support.annotation.RequiresApi
import com.example.ferna_xj0mjha.emonitor.model.entity.Analise
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IAnalise
import com.example.ferna_xj0mjha.emonitor.model.repository.ConsumoRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate

/**
 * Created by ferna_xj0mjha on 30/09/2017.
 */
class AnaliseConsumoPresenter(context: Context) : IAnalise.IPresenter {
    private var contexts: Context? = null
    private var view: IAnalise.IView? = null
    private var model: IAnalise.IModel? = null

    init {
        this.contexts = context
        model = ConsumoRepository(this)
    }

    override fun getContext(): Context = this.contexts!!

    override fun setView(view: IAnalise.IView) {
        this.view = view
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun makeAnalysis() {
        val consumo: Consumo = model?.findLastConsumo()!!
        val entries = ArrayList<PieEntry>()
        if (model?.accertEqualNull()!!)
            return

        consumo.dispositivos?.forEach { d ->
            val analises = RealmInstancia.getInstancia(contexts!!)
                    .where(Analise::class.java).greaterThanOrEqualTo("date", consumo.dataInicio!!)
                    .lessThanOrEqualTo("date", consumo.dataFim!!).equalTo("dispositovo.id", d.id).findAll()
            var sum: Number = analises.sum("valor")
            if (analises != null && analises.size > 0) {
                sum = sum.toFloat() * 1000 * 60 * 60
                entries.add(PieEntry(sum.toFloat(), d.nome!!))
            }
        }

        if (entries.size > 0) {
            val dataset = PieDataSet(entries, "# of Calls")
            dataset.setColors(*ColorTemplate.COLORFUL_COLORS)
            dataset.valueTextColor = Color.WHITE
            val data = PieData(dataset)
            view?.showAnalysis(data, consumo)
        }
    }
}