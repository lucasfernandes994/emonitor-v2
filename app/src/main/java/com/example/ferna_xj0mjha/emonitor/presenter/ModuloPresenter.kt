package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModulo
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import io.realm.RealmResults

/**
 * Created by lucas on 08/07/17.
 */

class ModuloPresenter(context: Context) : IModulo.IPresenter {
    private var view: IModulo.IView? = null
    private var model: IModulo.IModel? = null
    private var context: Context? = null

    init {
        this.context = context
    }

    override fun findModuloInDatabase() {
        val results: RealmResults<Modulo> = RealmInstancia.getInstancia(context!!).where(Modulo::class.java).findAll()
        if (results.size > 0)
            view?.setAdapter(results)
        else
            view?.exibirMensagemDeCadastroDeNovoDispositivo()
        view?.notifyAdapterDataChange()
    }

    override fun notifyAdapterDataChange() {
        view?.notifyAdapterDataChange()
    }

    override fun setView(view: IModulo.IView) {
        this.view = view
    }

    override fun getContext(): Context = context!!

    override fun showProgress() {
        view!!.showProgress()
    }

    override fun dimissProgress() {
        view!!.dimissProgress()
    }
}
