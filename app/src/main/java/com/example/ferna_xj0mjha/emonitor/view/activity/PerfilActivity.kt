package com.example.ferna_xj0mjha.emonitor.view.activity

import android.os.Bundle
import android.view.View
import android.widget.*

import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IPerfil
import com.example.ferna_xj0mjha.emonitor.presenter.PerfilPresenter

class PerfilActivity : BaseActivity(), IPerfil.IView {
    override fun showProgess() {
        progressBar?.visibility = View.VISIBLE
        nome?.visibility = View.GONE
        email?.visibility = View.GONE
        senha?.visibility = View.GONE
        confirmSenha?.visibility = View.GONE
        salvar?.visibility = View.GONE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
        nome?.visibility = View.VISIBLE
        email?.visibility = View.VISIBLE
        senha?.visibility = View.VISIBLE
        confirmSenha?.visibility = View.VISIBLE
        salvar?.visibility = View.VISIBLE
    }

    override fun atualizacaoBemsucedida() {
        Toast.makeText(applicationContext, "Perfil atualizado", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun atualizacaoComErro(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    private var nome: TextView? = null
    private var email: TextView? = null
    private var senha: TextView? = null
    private var confirmSenha: TextView? = null
    private var imBack: ImageView? = null
    private var salvar: Button? = null
    private var ivBack: ImageView? = null
    private var presenter: IPerfil.IPresenter? = null
    private var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perfil)
        initComponents()
        initHandlers()
        initPresenter()
    }

    fun initComponents() {
        nome = findViewById(R.id.et_nome)
        email = findViewById(R.id.et_email)
        senha = findViewById(R.id.et_senha)
        confirmSenha = findViewById(R.id.et_confirma_senha)
        imBack = findViewById(R.id.iv_back)
        salvar = findViewById(R.id.bt_salvar)
        ivBack = findViewById(R.id.imageView)
        progressBar = findViewById(R.id.progressBar)
    }

    fun initHandlers() {
        salvar?.setOnClickListener {
            presenter?.atulizarPerfil(nome?.text.toString(), email?.text.toString(), senha?.text.toString(), confirmSenha?.text.toString())
        }
        imBack?.setOnClickListener {
            onBackPressed()
        }

        ivBack?.setOnClickListener {
            onBackPressed()
        }
    }

    fun initPresenter() {
        this.presenter = PerfilPresenter(applicationContext)
        this.presenter?.setView(this)
    }
}
