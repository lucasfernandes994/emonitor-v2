package com.example.ferna_xj0mjha.emonitor.view.fragment


import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogout
import com.example.ferna_xj0mjha.emonitor.presenter.LogoutPresenter
import com.example.ferna_xj0mjha.emonitor.view.activity.LoginActivity
import com.example.ferna_xj0mjha.emonitor.view.activity.PerfilActivity
import com.example.ferna_xj0mjha.emonitor.view.activity.SobreActivity

class InfoFragment : BaseFragment(), ILogout.IView {
    private var tvEditarPerfil: TextView? = null
    private var tvSobre: TextView? = null
    private var tvLogout: TextView? = null
    private var presenter: ILogout.IPresenter? = null
    private var toolbar: Toolbar? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_info, container, false)
        initComponents(view)
        initHandlers()
        initPresenter()
        return view
    }

    private fun initComponents(view: View) {
        tvEditarPerfil = view.findViewById(R.id.tv_editar)
        tvSobre = view.findViewById(R.id.tv_sobre)
        tvLogout = view.findViewById(R.id.tv_logout)
        toolbar = view.findViewById(R.id.toolbar4)
    }

    private fun initHandlers() {
        tvEditarPerfil?.setOnClickListener { startActivity(Intent(context, PerfilActivity::class.java)) }
        tvSobre?.setOnClickListener { startActivity(Intent(context, SobreActivity::class.java)) }
        tvLogout?.setOnClickListener { presenter?.logout() }
    }

    fun initPresenter() {
        presenter = LogoutPresenter(context)
        presenter?.setView(this)
    }

    override fun logoutError() {
        Toast.makeText(context, "Não foi possível deslogar", Toast.LENGTH_SHORT).show()
    }

    override fun logoutSucess() {
        startActivity(Intent(context, LoginActivity::class.java))
        activity.finish()
    }

}
