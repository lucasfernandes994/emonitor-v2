package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.infra.SharedPreferencesManager
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogout

/**
 * Created by lucas on 02/09/2017.
 */
class LogoutPresenter(context: Context) : ILogout.IPresenter {
    private var context: Context? = null
    private var view: ILogout.IView? = null
    private var model: ILogout.IModel? = null

    init {
        this.model = SharedPreferencesManager
        this.context = context
    }

    override fun setView(view: ILogout.IView) {
        this.view = view
    }

    override fun getContext(): Context = this.context!!

    override fun logout() {
        try {
            model?.logout(this)
            logoutSucess()
        } catch (e: Exception) {
            logoutError()
        }
    }

    override fun logoutError() {
        view?.logoutError()
    }

    override fun logoutSucess() {
        view?.logoutSucess()
    }

}