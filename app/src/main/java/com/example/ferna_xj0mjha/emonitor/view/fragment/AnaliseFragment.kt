package com.example.ferna_xj0mjha.emonitor.view.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Calculo
import com.example.ferna_xj0mjha.emonitor.model.repository.ModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.TarifaRepository
import com.example.ferna_xj0mjha.emonitor.view.activity.AnaliseActivity
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.data.CombinedData
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import java.text.DecimalFormat

class AnaliseFragment : BaseFragment() {
    private var analise: Button? = null
    private var combinedChart: CombinedChart? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initBradcastAnalisysReceiver()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_analise, container, false)
        initComponents(view)
        initHandlers()
        initToolbar(view)
        try {
            initMainCombinedChart(context)
        } catch (e: IndexOutOfBoundsException) {
            Toast.makeText(context, "Adione um novo dispositivo para iniciar a nálise", Toast.LENGTH_SHORT).show()
        } catch (e: NullPointerException) {
            Log.e("Analise fragment", e.toString())
        }
        return view
    }

    fun initToolbar(view: View) {
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar3)
    }

    fun initComponents(v: View) {
        combinedChart = v.findViewById(R.id.main_combined_chart)
        analise = v.findViewById(R.id.button_analise)
    }

    fun initHandlers() {
        analise?.setOnClickListener {
            context.startActivity(Intent(context, AnaliseActivity::class.java))
        }
    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun initMainCombinedChart(context: Context) {
        combinedChart?.description?.text = "Valor gasto em R$"
        combinedChart?.setDrawGridBackground(true)
        combinedChart?.setDrawBarShadow(true)
        combinedChart?.isHighlightFullBarEnabled = true
        combinedChart?.setTouchEnabled(false)

        val l = combinedChart?.legend
        l?.isWordWrapEnabled = true
        l?.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        l?.horizontalAlignment = Legend.LegendHorizontalAlignment.CENTER
        l?.orientation = Legend.LegendOrientation.HORIZONTAL

        val leftAxis = combinedChart?.axisLeft
        leftAxis?.setDrawGridLines(false)
        leftAxis?.axisMinimum = 0f // this replaces setStartAtZero(true)

        val nomesDispositivos: ArrayList<String>? = getDispositivosNomes(context)
        val xAxis = combinedChart?.xAxis
        xAxis?.position = XAxis.XAxisPosition.BOTH_SIDED
        xAxis?.axisMinimum = 0f
        xAxis?.granularity = 1f
        xAxis?.valueFormatter = IAxisValueFormatter { value, _ -> nomesDispositivos?.get(value.toInt() % nomesDispositivos.size) }

        val data = CombinedData()
        data.setData(generateBarData(context))
        xAxis?.axisMaximum = data.xMax + 0.25f
        combinedChart?.data = data
        combinedChart?.invalidate()
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun generateBarData(context: Context): BarData {
        var entries = ArrayList<BarEntry>()
        entries = getBarEnteries(entries, context)

        val set1 = BarDataSet(entries, "Bar")
        set1.color = Color.rgb(127, 223, 252)
        set1.valueTextColor = Color.rgb(48, 169, 253)
        set1.valueTextSize = 10f
        set1.axisDependency = YAxis.AxisDependency.LEFT

        val barWidth = 0.45f // x2 dataset
        val d = BarData(set1)
        d.barWidth = barWidth

        return d
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun getBarEnteries(entries: ArrayList<BarEntry>, context: Context): ArrayList<BarEntry> {
        val results = ModuloRepository.findAll(context)
        val tarifa = TarifaRepository.findLastTarifa(context)
        var cont = 1
        if (tarifa != null) {
            results?.forEach { r ->
                val watts = if (r.dispositivo?.watts == null) 0.0 else r.dispositivo?.watts
                val valor = if (tarifa.valor == null) 0.0 else tarifa.valor
                val time = if (r.dispositivo?.time == null) 0 else r.dispositivo?.time
                val c = Calculo.calcularConsumo(watts!!, valor!!, time!!) * 1000 * 60 * 60
                val df = DecimalFormat("#.0000000000")
                df.format(c)
                c.toFloat().let { BarEntry(cont.toFloat(), it) }.let { entries.add(it) }
                cont++
            }
        }

        return entries
    }

    private fun getDispositivosNomes(context: Context): ArrayList<String> {
        val nomesDispositivos = ArrayList<String>()
        nomesDispositivos.add("")
        ModuloRepository.findAll(context)?.forEach { r ->
            r.dispositivo?.nome?.let { nomesDispositivos.add(it) }
        }
        return nomesDispositivos
    }

    private fun initBradcastAnalisysReceiver() {
        LocalBroadcastManager
                .getInstance(context)
                .registerReceiver(analisysReceiver,
                        IntentFilter(Constants.BroadcastReceiver.ANALISYS_RECEIVER))
    }

    private val analisysReceiver = object : BroadcastReceiver() {
        @RequiresApi(Build.VERSION_CODES.N)
        override fun onReceive(context: Context, intent: Intent) {
            try {
                initMainCombinedChart(context)
                combinedChart?.notifyDataSetChanged()
            } catch (e: Exception) {

            }
        }
    }
}
