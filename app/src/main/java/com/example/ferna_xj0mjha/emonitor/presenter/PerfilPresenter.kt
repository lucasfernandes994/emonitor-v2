package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IPerfil
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.model.service.PerfilServices
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback

/**
 * Created by ferna_xj0mjha on 19/09/2017.
 */

class PerfilPresenter(context: Context) : IPerfil.IPresenter {
    private var view: IPerfil.IView? = null
    private var context: Context? = null

    override fun setView(view: IPerfil.IView) {
        this.view = view
    }

    init {
        this.context = context
    }

    override fun getContext(): Context = context!!

    override fun atulizarPerfil(nome: String, email: String, senha: String, confirmSenha: String) {
        if (!nome.isEmpty() && !email.isEmpty() && !senha.isEmpty() && !confirmSenha.isEmpty()) {
            if (senha.equals(confirmSenha)) {
                val usuario = RealmInstancia.getInstancia(context!!).where(Usuario::class.java).equalTo("email", email).findFirst()
                if (usuario != null) {
                    view?.showProgess()
                    PerfilServices(this).atulizarPerfil(usuario, object : ICallback {
                        override fun callbackSucces() {
                            view?.hideProgress()
                            view?.atualizacaoBemsucedida()
                        }

                        override fun callbackError(mensagem: String) {
                            atualizacaoComErro(mensagem)
                            view?.hideProgress()
                        }
                    })
                } else
                    atualizacaoComErro("Usuário não encontrado.")
            } else {
                atualizacaoComErro("A senha deve ser igual.")
            }
        } else
            atualizacaoComErro("Preencha todos os campos.")
    }

    override fun atualizacaoBemsucedida() {
        this.view?.atualizacaoBemsucedida()
    }

    override fun atualizacaoComErro(mensagem: String) {
        this.view?.atualizacaoComErro(mensagem)
    }

}
