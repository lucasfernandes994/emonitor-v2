package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Tarifa

/**
 * Created by ferna_xj0mjha on 01/11/2017.
 */
interface ITarifa {
    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context

        fun salvarTarifa(valor: String)
        fun succesSave()
        fun errorSave(mensagem: String)
        fun setTarifa(valor: Double)

        fun save(tarifa: Tarifa)
        fun deleteAll()
        fun selectLast()
    }

    interface IView {
        fun succesSave()
        fun errorSave(mensagem: String)
        fun setTarifa(valor: Double)
    }

    interface IModel {
        fun save(tarifa: Tarifa)
        fun deleteAll()
        fun selectLast()
        fun nextId(): Int
    }
}