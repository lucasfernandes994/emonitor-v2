package com.example.ferna_xj0mjha.emonitor.infra
import android.content.Context
import android.content.SharedPreferences
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogout

/**
 * Created by lucas on 26/07/17.
 */

object SharedPreferencesManager : ILogout.IModel{

    override fun logout(presenter : ILogout.IPresenter) {
        clear(presenter.getContext())
    }

    fun getSharedPreference(context: Context): SharedPreferences =
            context.getSharedPreferences(Constants.Preferences.SHARED_PREFERENCE, 0)

    fun getBooleanValuePreference(key: String, context: Context): Boolean =
            getSharedPreference(context).getBoolean(key, java.lang.Boolean.FALSE)

    fun getStringValuePreference(key: String, context: Context): String =
            getSharedPreference(context).getString(key, null)

    fun savePreference(key: String, value: Boolean, context: Context) {
        val editor = getSharedPreference(context).edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    fun  savePreference(key: String, value: String?, context: Context) {
        val editor = getSharedPreference(context).edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun clear(context: Context) {
        val editor = getSharedPreference(context).edit()
        editor.clear().apply()
    }


}
