package com.example.ferna_xj0mjha.emonitor.model.service.calllback

import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo

/**
 * Created by ferna_xj0mjha on 12/10/2017.
 */
interface ICallbackModulo {
    fun callbackSucces(modulo: Modulo)
    fun callbackError(mensagem: String)
}