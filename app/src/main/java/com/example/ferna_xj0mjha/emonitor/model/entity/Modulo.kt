package com.example.ferna_xj0mjha.emonitor.model.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by lucas on 08/07/17.
 */

open class Modulo : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var nome: String? = null
    var ip: String? = null
    @SerializedName("macAdress")
    @Expose
    var macAdress: String? = null
    var classe: String? = null
    var porta: String? = null
    @SerializedName("status")
    @Expose
    var  status: String? = null
    @SerializedName("dispositivo")
    @Expose
    var dispositivo: Dispositivo? = null
    @SerializedName("sincronazed")
    @Expose
    var sincronazed: Int? = null
}