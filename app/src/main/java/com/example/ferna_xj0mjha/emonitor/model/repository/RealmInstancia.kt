package com.example.ferna_xj0mjha.emonitor.model.repository

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.SharedPreferencesManager
import io.realm.Realm
import io.realm.RealmConfiguration


/**
 * Created by lucas on 26/07/17.
 */

object RealmInstancia {
    private var realmConfiguration: RealmConfiguration? = null
    private val currentVersion = 5

    fun getInstancia(context: Context): Realm {

        val databaseName = "EMonitor" + SharedPreferencesManager.getStringValuePreference(Constants.UserData.USER_DATABASE, context)
        if (realmConfiguration == null || databaseName != realmConfiguration!!.realmFileName) {
            Realm.init(context)
            realmConfiguration = RealmConfiguration
                    .Builder()
                    .name(databaseName)
                    .schemaVersion(currentVersion.toLong())
                    .deleteRealmIfMigrationNeeded()
                    .build()
            Realm.setDefaultConfiguration(realmConfiguration!!)
        }
        return Realm.getDefaultInstance()
    }
}
