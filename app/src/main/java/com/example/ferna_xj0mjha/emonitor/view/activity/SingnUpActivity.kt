package com.example.ferna_xj0mjha.emonitor.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ISingUp
import com.example.ferna_xj0mjha.emonitor.presenter.SingnUpPresenter

class SingnUpActivity : BaseActivity(), ISingUp.IView {

    var etNome: TextView? = null
    var etEmail: TextView? = null
    var etSenha: TextView? = null
    var btSingnUp: Button? = null
    var presenter: ISingUp.IPresenter? = null
    var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setContentView(R.layout.activity_signup)
        super.onCreate(savedInstanceState)
        initComponents()
        initHandlers()
        setPresenter()
    }

    fun initComponents() {
        etNome = findViewById(R.id.et_nome)
        etEmail = findViewById(R.id.et_email)
        etSenha = findViewById(R.id.et_senha)
        btSingnUp = findViewById(R.id.bt_signup)
        progressBar = findViewById(R.id.progressBar)
    }

    fun initHandlers() {
        btSingnUp?.setOnClickListener {
            presenter?.createAccount(etNome?.text?.toString()?.trim()!!, etEmail?.text?.toString()?.trim()!!, etSenha?.text?.toString()?.trim()!!)
        }
    }

    private fun setPresenter() {
        presenter = SingnUpPresenter(applicationContext)
        presenter?.setView(this)
    }

    override fun accountSucess() {
        startActivity(Intent(this, MainActivity::class.java))
    }

    override fun accountError(erro: String) {
        Toast.makeText(this, erro, Toast.LENGTH_SHORT).show()
    }

    override fun errorEmail(erro: String) {
        Toast.makeText(this, erro, Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    @SuppressLint("ResourceAsColor")
    override fun showProgress() {
        progressBar?.indeterminateDrawable?.setColorFilter(R.color.colorPrimary, android.graphics.PorterDuff.Mode.SRC_ATOP)
        progressBar?.visibility = View.VISIBLE

    }
}
