package com.example.ferna_xj0mjha.emonitor.infra.rest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ferna_xj0mjha on 26/08/2017.
 */
class Mensagem {
    @SerializedName("mensagem")
    @Expose
    var mensagem: String? = null
}