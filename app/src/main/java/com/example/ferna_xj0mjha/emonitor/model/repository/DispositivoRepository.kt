package com.example.ferna_xj0mjha.emonitor.model.repository

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo

/**
 * Created by ferna_xj0mjha on 02/11/2017.
 */
class DispositivoRepository {
    companion object {
        fun nextId(context: Context): Int {
            return try {
                RealmInstancia.getInstancia(context).where(Dispositivo::class.java).max("id").toInt() + 1
            } catch (e: ArrayIndexOutOfBoundsException) {
                0
            } catch (ex: NullPointerException) {
                0
            }
        }
    }
}