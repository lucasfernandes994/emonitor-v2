package com.example.ferna_xj0mjha.emonitor.model.abstracts

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumoRepository

/**
 * Created by ferna_xj0mjha on 27/09/2017.
 */
abstract class AbsConsumo {
    open fun deleteAllFromRealm(context: Context) {}
    open fun save(context: Context, consumo: Consumo) {}
    open fun selectLast(context: Context): Consumo? = null
    open fun deleteFromRealm(context: Context, consumo: Consumo, presenter: IConsumoRepository.IPresenter) {}
}