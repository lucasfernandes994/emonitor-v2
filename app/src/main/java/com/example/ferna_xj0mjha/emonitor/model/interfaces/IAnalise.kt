package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.github.mikephil.charting.data.PieData

/**
 * Created by ferna_xj0mjha on 30/09/2017.
 */
interface IAnalise {
    interface IPresenter {
        fun getContext(): Context
        fun setView(view: IView)

        fun makeAnalysis()
    }

    interface IView {
        fun makeAnalysis()
        fun showAnalysis(pieData: PieData, consumo: Consumo)
        fun reportError(mensagem: String)
    }

    interface IModel {
        fun findLastConsumo(): Consumo
        fun accertEqualNull() : Boolean
    }
}