package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloService
import com.example.ferna_xj0mjha.emonitor.model.service.ModuloService
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackModulo
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallbackStatusModulo

/**
 * Created by ferna_xj0mjha on 12/10/2017.
 */
class ModuloServicePresenter(context: Context) : IModuloService.IPresenter {
    private var context: Context? = null
    private var view: IModuloService.IView? = null
    private var model: IModuloService.IModel? = null

    init {
        this.model = ModuloService(this)
        this.context = context
    }

    override fun setView(view: IModuloService.IView) {
        this.view = view
    }

    override fun getContext(): Context = context!!

    override fun findModuloData(modulo: Modulo, callback: ICallbackModulo) {
        model?.findModuloData(modulo, object : ICallbackModulo {
            override fun callbackSucces(moduloData: Modulo) {
                callback.callbackSucces(modulo)
            }

            override fun callbackError(mensagem: String) {
                callback.callbackError(mensagem)
            }

        })
    }

    override fun turnOff(modulo: Modulo) {
        this.showProgress(context?.getString(R.string.desligando_dispositivo)!!, context?.getString(R.string.desligando)!!)
        model?.turnOff(modulo, object : ICallback {
            override fun callbackSucces() {
                dismissProgress()
                succesTurnOff()
            }

            override fun callbackError(mensagem: String) {
                dismissProgress()
                errorTurnOff(mensagem)
            }

        })
    }

    override fun turnOn(modulo: Modulo) {
        this.showProgress(context?.getString(R.string.ligando_dispositivo)!!, context?.getString(R.string.ligando)!!)
        model?.turnOn(modulo, object : ICallback {
            override fun callbackSucces() {
                dismissProgress()
                succesTurnOn()
            }

            override fun callbackError(mensagem: String) {
                dismissProgress()
                errorTurnOn(mensagem)
            }

        })
    }

    override fun findStatus(modulo: Modulo) {
        model?.findStatus(modulo, object : ICallbackStatusModulo {
            override fun callbackSucces(mensagem: String) {
                succesStatus(mensagem.equals("on"))
            }

            override fun callbackError(mensagem: String) {
                errorStatus(mensagem)
            }

        })
    }

    override fun ping(modulo: Modulo) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showMessage(mensagem: String) {
        view?.showMessage(mensagem)
    }

    override fun showModuloData() {
        view?.showModuloData()
    }

    override fun succesTurnOff() {
        view?.succesTurnOff()
    }

    override fun errorTurnOff(mensagem: String) {
        view?.errorTurnOff(mensagem)
    }

    override fun succesTurnOn() {
        view?.succesTurnOn()
    }

    override fun errorTurnOn(mensagem: String) {
        view?.errorTurnOn(mensagem)
    }

    override fun successPing() {
        view?.successPing()
    }

    override fun errorPing(mensagem: String) {
        view?.errorPing(mensagem)
    }

    override fun succesStatus(value: Boolean) {
        view?.succesStatus(value)
    }

    override fun errorStatus(mensagem: String) {
        view?.errorStatus(mensagem)
    }

    override fun showProgress(mensagem: String, title: String) {
        view?.showProgress(mensagem, title)
    }

    override fun dismissProgress() {
        view?.dismissProgress()
    }
}