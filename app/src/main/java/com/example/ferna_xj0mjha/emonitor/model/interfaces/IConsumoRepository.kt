package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo

/**
 * Created by ferna_xj0mjha on 07/10/2017.
 */
interface IConsumoRepository {
    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context

        fun deleteConsumo(consumo: Consumo)

        fun sucessDelete()
        fun errorDelete()
    }

    interface IView {
        fun sucessDelete()
        fun errorDelete()
    }
}