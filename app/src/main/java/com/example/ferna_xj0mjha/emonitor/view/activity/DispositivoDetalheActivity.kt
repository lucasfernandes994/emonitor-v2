package com.example.ferna_xj0mjha.emonitor.view.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Calculo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloService
import com.example.ferna_xj0mjha.emonitor.model.repository.ModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.TarifaRepository
import com.example.ferna_xj0mjha.emonitor.model.service.ModuloService
import com.example.ferna_xj0mjha.emonitor.presenter.ModuloRepositoryPresenter
import com.example.ferna_xj0mjha.emonitor.presenter.ModuloServicePresenter
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import org.json.JSONException
import org.json.JSONObject
import java.text.DecimalFormat

class DispositivoDetalheActivity : BaseActivity(), IModuloService.IView {
    private var toolbar: Toolbar? = null
    private var tvWatt: TextView? = null
    private var tvValor: TextView? = null
    private var tvTempo: TextView? = null
    private var tvCorrente: TextView? = null
    private var modulo: Modulo? = null
    private var presenterRepository: IModuloRepository.IPresenter? = null
    private var presenterService: IModuloService.IPresenter? = null
    private var buttonTurn: Button? = null
    private var progressDialog: ProgressDialog? = null
    private var valorTarifa: Double? = null

    private var mainTaskStop = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dispositivo_detalhe)
        initComponents()
        initPresenter()
        setModulo()
        intHandlers()
        initDialog()
    }

    override fun onStart() {
        super.onStart()
        mainTaskStop = true
        //presenterService?.findStatus(modulo!!)
        findModuloData(modulo!!)
    }

    override fun onResume() {
        super.onResume()
        setModulo()
        mainTaskStop = true
    }

    override fun onPause() {
        super.onPause()
        mainTaskStop = false
    }

    override fun onDestroy() {
        super.onDestroy()
        mainTaskStop = false
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater? = menuInflater
        inflater?.inflate(R.menu.menu_detalhe_dispositivo, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId
        when (id) {
            R.id.editar -> {
                val it = Intent(applicationContext, CadastroDispositivoActivity::class.java)
                it.putExtra(Constants.IntentParam.ID_MODULO, modulo?.id)
                startActivity(it)
            }
            16908332 -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun initComponents() {
        this.toolbar = findViewById(R.id.toolbar5)
        this.toolbar!!.setNavigationIcon(R.drawable.ic_arrow_back_white_36dp)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.toolbar?.textAlignment = View.TEXT_ALIGNMENT_CENTER
        }
        setSupportActionBar(toolbar)
        this.tvWatt = findViewById(R.id.tv_watt)
        this.tvValor = findViewById(R.id.tv_valor)
        this.tvTempo = findViewById(R.id.tv_tempo)
        this.tvCorrente = findViewById(R.id.tv_corrente)
        this.buttonTurn = findViewById(R.id.button_turn)
    }

    fun setModulo() {
        val bundle = intent.extras
        var corrente = "0000 A"
        var wat = "0000 W"
        val tarifa = TarifaRepository.findLastTarifa(applicationContext)
        this.valorTarifa = tarifa.valor

        if (bundle != null && this.valorTarifa != null && tarifa.valor != null) {
            val id = bundle.get(Constants.IntentParam.ID_MODULO) as Int
            this.modulo = presenterRepository?.findModuloById(id, applicationContext)
            this.toolbar?.title = modulo?.dispositivo?.nome

            if (modulo?.dispositivo != null && modulo?.dispositivo?.corrente != null)
                corrente = modulo?.dispositivo?.corrente?.toString() + " A"
            this.tvCorrente?.text = corrente

            if (modulo?.status != null && modulo?.status?.equals("on")!!)
                this.succesStatus(true)
            else
                this.succesStatus(false)

            if (modulo?.dispositivo?.watts != null && modulo?.dispositivo?.time != null) {
                val watts = modulo?.dispositivo?.watts!!
                val valor = valorTarifa!!
                val time = modulo?.dispositivo?.time!!
                val c = Calculo.calcularConsumo(watts, valor, time) * 1000 * 60 * 60
                val df = DecimalFormat("###.0000000000")
                df.format(c)
                val results = "R$ 0" + df.format(c).toString()
                this.tvValor?.text = results
                wat = modulo?.dispositivo?.watts?.toString() + " W"
                this.tvTempo?.text = Calculo.millisToTime(modulo?.dispositivo?.time!!)
            }
        }
        this.tvWatt?.text = wat
    }

    fun intHandlers() {
        buttonTurn?.setOnClickListener {
            if (modulo?.status != null && modulo?.status?.equals("on")!!)
                presenterService?.turnOff(modulo!!)
            else
                presenterService?.turnOn(modulo!!)
        }
    }

    fun initPresenter() {
        this.presenterRepository = ModuloRepositoryPresenter(applicationContext)
        this.presenterService = ModuloServicePresenter(applicationContext)
        this.presenterService?.setView(this)
    }

    fun initDialog() {
        progressDialog = ProgressDialog(this@DispositivoDetalheActivity)
        progressDialog?.setProgressStyle(ProgressDialog.STYLE_SPINNER)
    }

    override fun showProgress(mensagem: String, title: String) {
        progressDialog?.setMessage(mensagem)
        progressDialog?.setTitle(title)
        progressDialog?.show()
    }

    override fun dismissProgress() {
        if (progressDialog != null)
            progressDialog?.dismiss()
    }

    override fun showMessage(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun showModuloData() {
        modulo = ModuloRepositoryPresenter(applicationContext).findModuloById(modulo?.id!!, applicationContext)
        tvTempo?.text = modulo?.dispositivo?.time.toString()
    }

    override fun succesTurnOff() {
        succesStatus(false)
    }

    override fun errorTurnOff(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun succesTurnOn() {
        succesStatus(true)
    }

    override fun errorTurnOn(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun successPing() {
        Toast.makeText(applicationContext, "Dispositivo encontrado.", Toast.LENGTH_SHORT).show()
    }

    override fun errorPing(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun succesStatus(value: Boolean) {
        if (value) {
            buttonTurn?.text = "Ligado"
            buttonTurn?.setTextColor(resources.getColor(R.color.white))
            buttonTurn?.setBackgroundResource(R.drawable.retangular_ripple_gradient)
        } else {
            buttonTurn?.text = "Desligado"
            buttonTurn?.setTextColor(resources.getColor(R.color.colorPrimary))
            buttonTurn?.setBackgroundResource(R.drawable.retangular_ripple_blue)
        }
        modulo = modulo?.id?.let { ModuloRepository.findById(applicationContext, it) }
    }

    override fun errorStatus(mensagem: String) {
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("StaticFieldLeak")
    private fun findModuloData(modulo: Modulo) {
        val param = Constants.ParametroModulo.GET_MODULO
        val port = modulo.porta
        val ip = modulo.ip
        val id = modulo.id
        if (mainTaskStop) {
            object : AsyncTask<Void, Void, String>() {
                override fun doInBackground(vararg p0: Void?): String = ModuloService().sendCommand(param, ip!!, port!!, param)

                override fun onPostExecute(result: String?) {
                    super.onPostExecute(result)
                    try {
                        val moduloData = Gson().fromJson(JSONObject(result).getJSONObject("mensagem").getJSONObject("modulo").toString(), Modulo::class.java)
                        moduloData.porta = port
                        moduloData.ip = ip
                        id?.let { ModuloRepository.saveModuloWithBackgroundRealm(moduloData, this@DispositivoDetalheActivity, it) }
                        Handler().postDelayed({
                            setModulo()
                            findModuloData(modulo)
                        }, 1000)
                    } catch (ex: java.lang.IllegalStateException) {
                        ex.printStackTrace()
                    } catch (ex: JSONException) {
                        ex.printStackTrace()
                    } catch (ex: JsonSyntaxException) {
                        ex.printStackTrace()
                    } finally {
                        this.cancel(true)
                    }
                }
            }.execute()
        }
    }
}
