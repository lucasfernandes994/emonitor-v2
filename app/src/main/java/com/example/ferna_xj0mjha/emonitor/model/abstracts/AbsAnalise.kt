package com.example.ferna_xj0mjha.emonitor.model.abstracts

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import io.realm.RealmResults

/**
 * Created by ferna_xj0mjha on 27/09/2017.
 */
abstract class AbsAnalise {
    abstract fun findAllModulo(context: Context) : RealmResults<Modulo>
    abstract fun deleteAllFromRealm(context: Context)
}