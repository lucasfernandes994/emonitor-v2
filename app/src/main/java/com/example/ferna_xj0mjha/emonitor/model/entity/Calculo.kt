package com.example.ferna_xj0mjha.emonitor.model.entity

import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by lucas on 30/09/2017.
 */
open class Calculo {
    companion object {
        fun calcularConsumo(watts: Double, tarifaKwh: Double, millis: Long): Double {
            val wattsEmMillessegundos = (watts / 1000) / 60 / 60 / 1000
            val precoTarifaEmMilessegundos = tarifaKwh / 60 / 60 / 1000
            return (wattsEmMillessegundos * millis) * precoTarifaEmMilessegundos
        }

        fun millisToTime(millis: Long): String {
            val date = Date(millis)
            val formatter = SimpleDateFormat("HH:mm:ss:SSS")
            val cal = Calendar.getInstance() // creates calendar
            cal.time = date // sets calendar time/date
            cal.add(Calendar.HOUR_OF_DAY, -21) // adds one hour
            return formatter.format(cal.time)// returns new date object, one hour in the future
        }
    }
}