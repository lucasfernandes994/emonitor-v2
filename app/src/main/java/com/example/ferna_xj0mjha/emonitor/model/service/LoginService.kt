package com.example.ferna_xj0mjha.emonitor.model.service

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.SharedPreferencesManager
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogin
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.google.gson.Gson
import com.loopj.android.http.*
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by lucas on 02/08/2017.
 */
class LoginService(context: Context, presenter: ILogin.IPresenter) : ILogin.IModel {
    private var context: Context
    private var presenter: ILogin.IPresenter? = null

    override fun `validCredential`(usuario: Usuario, callback: ICallback) {
        val client = AsyncHttpClient()
        val jsonParams = JSONObject()
        jsonParams.put("email", usuario.email)
        jsonParams.put("senha", usuario.senha)
        val stringEntity = StringEntity(jsonParams.toString())
        client.post(context, Constants.Url.LOGIN_URL + "login", stringEntity, "application/json", object : JsonHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONArray?) {
                if (statusCode == 200) {
                    try {
                        val usuario: Array<Usuario> = Gson().fromJson(response.toString(), Array<Usuario>::class.java)
                        saveUserPreference(usuario[0])
                        usuario.forEach { user: Usuario ->
                            RealmInstancia.getInstancia(context).executeTransaction { realm -> realm.copyToRealmOrUpdate(user) }
                        }
                        callback.callbackSucces()

                    } catch (e: Exception) {
                        callback.callbackError("Usuário inválido.")
                    }
                } else {
                    callback.callbackError("Conexão perdida.")
                }
            }

            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                callback.callbackError(response?.getString("mensagem")!!)
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                callback.callbackError("Conexão perdida.")
            }

            override fun onCancel() {
                super.onCancel()
                callback.callbackError("Conexão perdida.")
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONArray?) {
                super.onFailure(statusCode, headers, throwable, errorResponse)
                callback.callbackError("Conexão perdida.")
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                super.onFailure(statusCode, headers, throwable, errorResponse)
                callback.callbackError("Conexão perdida.")
            }

        })
    }

    override fun saveUserPreference(usuario: Usuario) {
        SharedPreferencesManager.savePreference(Constants.UserData.USER_DATABASE, usuario.email, context)
        SharedPreferencesManager.savePreference(Constants.Preferences.USER_ACTIVE, true, context)
    }

    init {
        this.presenter = presenter
        this.context = context
    }
}