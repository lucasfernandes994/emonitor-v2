package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback

/**
 * Created by lucas on 27/07/17.
 */

interface ILogin {

    interface IPresenter {
        fun setView(view: IView)
        fun getContext() : Context

        fun userLogged()
        fun validCredential(email: String, senha: String)
        fun loginSucess()
        fun loginError(mensagem : String)
    }

    interface IModel {
        fun validCredential(usuario: Usuario, callback : ICallback)

        fun saveUserPreference(usuario: Usuario)
    }

    interface IView {
        fun loginSucess()
        fun loginError(mensagem : String)
        fun hideProgress()
        fun showProgress()
    }

}
