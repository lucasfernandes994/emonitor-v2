package com.example.ferna_xj0mjha.emonitor.model.service.calllback

/**
 * Created by lucas on 12/10/2017.
 */
interface ICallbackStatusModulo {
    fun callbackSucces(mensagem: String)
    fun callbackError(mensagem: String)
}