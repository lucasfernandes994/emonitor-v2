package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Tarifa
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ITarifa
import com.example.ferna_xj0mjha.emonitor.model.repository.TarifaRepository

/**
 * Created by ferna_xj0mjha on 01/11/2017.
 */
class TarifaPresenter(context: Context) : ITarifa.IPresenter {
    var iview: ITarifa.IView? = null
    var icontext: Context? = null
    var imodel: ITarifa.IModel? = null

    init {
        imodel = TarifaRepository(this)
        this.icontext = context
    }

    override fun setView(view: ITarifa.IView) {
        this.iview = view
    }

    override fun getContext(): Context = this.icontext!!

    override fun salvarTarifa(valor: String) {
        if (valor != "") {
            val tarifa = Tarifa()
            tarifa.id = imodel?.nextId()
            tarifa.valor = valor.toDouble()
            deleteAll()
            save(tarifa)
        } else
            errorSave(icontext?.resources?.getString(R.string.valor_invalido)!!)
    }

    override fun succesSave() {
        iview?.succesSave()
    }

    override fun errorSave(mensagem: String) {
        iview?.errorSave(mensagem)
    }

    override fun setTarifa(valor: Double) {
        iview?.setTarifa(valor)
    }

    override fun save(tarifa: Tarifa) {
        imodel?.save(tarifa)
    }

    override fun deleteAll() {
        imodel?.deleteAll()
    }

    override fun selectLast() {
        imodel?.selectLast()
    }
}