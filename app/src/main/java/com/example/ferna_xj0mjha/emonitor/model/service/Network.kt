package com.example.ferna_xj0mjha.emonitor.model.service

import android.content.Context
import android.net.ConnectivityManager
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.INetwork


/**
 * Created by lucas on 08/07/17.
 */

class Network : INetwork.IModel {
    private var moduloPresenter: IModulo.IPresenter? = null
    private var networkPresenter: INetwork.IPresenter? = null

    constructor(presenter: IModulo.IPresenter) {
        this.moduloPresenter = presenter
    }

    constructor(networkPresenter: INetwork.IPresenter) {
        this.networkPresenter = networkPresenter
    }

    override fun isNetworkAvailable(context: Context) {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        if (activeNetworkInfo != null)
            networkPresenter?.networkError()

        if (activeNetworkInfo!!.isConnected)
            networkPresenter?.noNetwork()
    }
}
