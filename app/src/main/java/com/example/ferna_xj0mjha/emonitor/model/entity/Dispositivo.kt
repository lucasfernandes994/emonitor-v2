package com.example.ferna_xj0mjha.emonitor.model.entity

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import java.util.*

/**
 * Created by lucas on 08/07/17.
 */


open class Dispositivo : RealmObject() {
    var id: Int? = null
    var nome: String? = null
    var descricao: String? = null
    var pathImage: String? = null
    @SerializedName("watts")
    var watts: Double? = null
    @SerializedName("time")
    var time: Long? = null
    @SerializedName("corrente")
    var corrente: Double? = null
    var lastTimeSincronized: Long? = null
    var dataInicio: Date? = null
    var dataFim: Date? = null
}
