package com.example.ferna_xj0mjha.emonitor.view.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.LocalBroadcastManager
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.INetwork
import com.example.ferna_xj0mjha.emonitor.presenter.ModuloPresenter
import com.example.ferna_xj0mjha.emonitor.view.activity.CadastroDispositivoActivity
import com.example.ferna_xj0mjha.emonitor.view.activity.DispositivoDetalheActivity
import com.example.ferna_xj0mjha.emonitor.view.adapter.ModuloAdapter
import io.realm.RealmResults
import java.util.concurrent.ExecutionException


/**
 * Created by lucas on 08/07/17.
 */

class HomeFragment : BaseFragment(), IModulo.IView, INetwork.IView {
    private var recyclerView: RecyclerView? = null
    private var moduloPresenter: IModulo.IPresenter? = null
    private var refresh_modulo: SwipeRefreshLayout? = null
    private var moduloAdapter: ModuloAdapter? = null
    private var floatButton: FloatingActionButton? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home, container, false)
        init(view)
        initRecyclerView()
        initHandlers()
        initToolbar(view)
        moduloPresenter = ModuloPresenter(context)
        moduloPresenter?.setView(this)
        try {
            moduloPresenter?.findModuloInDatabase()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return view
    }

    private fun init(view: View) {
        recyclerView = view.findViewById(R.id.rv_modulo)
        refresh_modulo = view.findViewById(R.id.sr_refresh_modulos)
        floatButton = view.findViewById(R.id.floatingActionButton2)
    }

    private fun initHandlers() {
        refresh_modulo?.setOnRefreshListener {
            moduloPresenter?.findModuloInDatabase()
        }
        floatButton?.setOnClickListener {
            startActivity(Intent(context, CadastroDispositivoActivity::class.java))
        }

        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 && floatButton?.visibility === View.VISIBLE) {
                    floatButton?.hide()
                } else if (dy < 0 && floatButton?.visibility !== View.VISIBLE) {
                    floatButton?.show()
                }
            }
        })
    }

    private fun initRecyclerView() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        recyclerView?.layoutManager = layoutManager
        LocalBroadcastManager.getInstance(context).registerReceiver(mMessageReceiver,
                IntentFilter(Constants.BroadcastReceiver.REFRESH_MODULO_ADAPTER))
    }

    override fun onStart() {
        super.onStart()
    }

    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            moduloAdapter?.notifyDataSetChanged()
        }
    }


    override fun setAdapter(modulos: RealmResults<Modulo>) {
        val onClickListener = View.OnClickListener { v ->
            val itemPosition = recyclerView?.getChildLayoutPosition(v)
            val intent = Intent(activity, DispositivoDetalheActivity::class.java)
            val id = moduloAdapter?.getItem(itemPosition!!)?.id
            intent.putExtra(Constants.IntentParam.ID_MODULO, id)
            context?.startActivity(intent)
        }
        moduloAdapter = ModuloAdapter(modulos, true, context, onClickListener)
        recyclerView?.adapter = moduloAdapter
    }

    override fun notifyAdapterDataChange() {
        moduloAdapter?.notifyDataSetChanged()
        refresh_modulo?.isRefreshing = false
    }

    private fun initToolbar(view: View) {
        val toolbar: Toolbar = view.findViewById(R.id.toolbar3)
    }

    override fun networkError() {
        Toast.makeText(context, "Erro ao conectar.", Toast.LENGTH_SHORT).show()
    }

    override fun noNetwork() {
        Toast.makeText(context, "Sem conexão.", Toast.LENGTH_SHORT).show()
    }

    override fun exibirMensagemDeCadastroDeNovoDispositivo() {
        Toast.makeText(context, "Adicione um novo dispositivos...", Toast.LENGTH_SHORT).show()
    }

    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun dimissProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(context).unregisterReceiver(mMessageReceiver);
        super.onDestroy()
    }

}
