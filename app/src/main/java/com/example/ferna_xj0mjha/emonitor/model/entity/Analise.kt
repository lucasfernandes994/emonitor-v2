package com.example.ferna_xj0mjha.emonitor.model.entity

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by ferna_xj0mjha on 29/11/2017.
 */
open class Analise : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var date: Date? = null
    var dispositovo: Dispositivo? = null
    var valor: Double? = null
}