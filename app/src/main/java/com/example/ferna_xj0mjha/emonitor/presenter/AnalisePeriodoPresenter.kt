package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumo
import com.example.ferna_xj0mjha.emonitor.model.repository.ConsumoRepository
import java.util.*

/**
 * Created by ferna_xj0mjha on 30/09/2017.
 */
class AnalisePeriodoPresenter(context: Context) : IConsumo.IPresenter {
    var model: IConsumo.IModel? = null
    var views: IConsumo.IView? = null
    var contexts: Context? = context
    var consumos: Consumo? = null

    init {
        this.model = ConsumoRepository(this)
    }

    override fun setView(view: IConsumo.IView) {
        this.views = view
    }

    override fun getContext(): Context = contexts!!

    override fun genereteMultipleReport(dataInicio: Date, datFim: Date, valorKwh: Double) {
        if (!dataInicio.equals("") && !datFim.equals("")) {
            if (datFim >= dataInicio)
                if (consumos != null && consumos?.dispositivos != null && consumos?.dispositivos?.size!! > 0)
                    this.model?.genereteMultipleReport(consumos!!, dataInicio, datFim)
                else
                    this.views?.showReportMessage("Nenhum dispositivo selecionado.")
            else
                this.views?.showReportMessage("A data fim não pode menor que a data de início.")

        } else
            this.views?.showReportMessage("Preencha a data correntamente.")
    }

    override fun showReport(boolean: Boolean) {
        if (boolean)
            this.views?.showReport()
        else
            this.views?.showReportMessage("Não foram encontrados dispositivos neste período informado.")
    }

    override fun createNew() {
        this.model?.createNew()
    }

    override fun setConsumo(consumo: Consumo) {
        this.consumos = consumo
        this.views?.setConsumo(consumo)
    }

    override fun deleteConsumo(consumo: Consumo) {
        this.model?.deleteConsumo(consumo)
    }

    override fun susccesDelete() {
        this.views?.susccesDelete()
    }

    override fun errorDelete(mensagem: String) {
        this.views?.errorDelete(mensagem)
    }

    override fun selectLast() {
        this.model?.selectLast()
    }
}