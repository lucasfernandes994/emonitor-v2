package com.example.ferna_xj0mjha.emonitor.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter

/**
 * Created by ferna_xj0mjha on 27/09/2017.
 */
class ModuloAnaliseAdapter(data: OrderedRealmCollection<Modulo>?, autoUpdate: Boolean, context: Context, consumo: Consumo) : RealmRecyclerViewAdapter<Modulo, ModuloAnaliseAdapter.ModuloViewHolder>(data, autoUpdate) {
    var context: Context? = null
    var consumo: Consumo? = null

    init {
        this.context = context
        this.consumo = consumo
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ModuloViewHolder =
            ModuloViewHolder(LayoutInflater.from(context!!).inflate(R.layout.item_modulo_analise, parent, false))

    override fun onBindViewHolder(holder: ModuloViewHolder?, position: Int) {
        holder?.nome?.setText(getItem(position)?.dispositivo?.nome)
        if (consumo?.dispositivos != null && consumo?.dispositivos?.contains(getItem(position)?.dispositivo)!!)
            holder?.check?.isChecked = true
        holder?.check?.setOnCheckedChangeListener({ buttonView, isChecked ->
            if (isChecked)
                RealmInstancia.getInstancia(context!!).executeTransaction { realm ->
                    consumo?.dispositivos?.add(getItem(position)?.dispositivo!!)
                    realm?.copyToRealmOrUpdate(consumo)
                }
            else
                RealmInstancia.getInstancia(context!!).executeTransaction { realm ->
                    consumo?.dispositivos?.remove(getItem(position)?.dispositivo!!)
                    realm?.copyToRealmOrUpdate(consumo)
                }
        })
    }

    class ModuloViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var nome: TextView? = itemView?.findViewById(R.id.tv_nome)
        var check: CheckBox? = itemView?.findViewById(R.id.checkBox)
    }
}

