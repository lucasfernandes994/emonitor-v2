package com.example.ferna_xj0mjha.emonitor.model.service

import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.infra.rest.Mensagem
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IPerfil
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.google.gson.Gson
import com.loopj.android.http.AsyncHttpClient
import com.loopj.android.http.JsonHttpResponseHandler
import cz.msebera.android.httpclient.Header
import cz.msebera.android.httpclient.entity.StringEntity
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by ferna_xj0mjha on 19/09/2017.
 */
class PerfilServices(presenter: IPerfil.IPresenter) : IPerfil.IModel {
    private var presenter: IPerfil.IPresenter? = null

    init {
        this.presenter = presenter
    }

    override fun atulizarPerfil(usuario: Usuario, callback: ICallback) {
        val client = AsyncHttpClient()
        val jsonParams = JSONObject()
        jsonParams.put("nome", usuario.nome)
        jsonParams.put("email", usuario.email)
        jsonParams.put("senha", usuario.senha)
        val stringEntity = StringEntity(jsonParams.toString())
        client.put(presenter?.getContext(), Constants.Url.LOGIN_URL + "usuario", stringEntity, "application/json", object : JsonHttpResponseHandler() {
            override fun onSuccess(statusCode: Int, headers: Array<out Header>?, response: JSONObject?) {
                if (statusCode == 200) {
                    try {
                        val mensagem: Mensagem = Gson().fromJson(response.toString(), Mensagem::class.java)
                        if (mensagem.mensagem?.equals("sucess")!!) {
                            RealmInstancia.getInstancia(presenter?.getContext()!!).executeTransaction { realm -> realm.copyToRealmOrUpdate(usuario) }
                            callback.callbackSucces()
                        } else {
                            callback.callbackError(mensagem.mensagem!!)
                        }
                    } catch (e: Exception) {
                        callback.callbackError("Erro ao atualizoar.")
                    }
                }
            }

            override fun onCancel() {
                callback.callbackError("Erro ao atualizoar.")
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, responseString: String?, throwable: Throwable?) {
                callback.callbackError("Erro ao atualizoar.")
            }

            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONArray?) {
                callback.callbackError("Erro ao atualizoar.")
            }


            override fun onFailure(statusCode: Int, headers: Array<out Header>?, throwable: Throwable?, errorResponse: JSONObject?) {
                callback.callbackError("Erro ao atualizoar.")
            }
        })
    }
}