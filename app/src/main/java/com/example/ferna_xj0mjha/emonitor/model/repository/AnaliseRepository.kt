package com.example.ferna_xj0mjha.emonitor.model.repository

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Analise
import com.example.ferna_xj0mjha.emonitor.model.entity.Calculo
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo
import com.example.ferna_xj0mjha.emonitor.model.entity.Tarifa
import io.realm.Realm
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by ferna_xj0mjha on 29/11/2017.
 */
class AnaliseRepository {
    companion object {
        fun nextId(context: Context): Int {
            return try {
                RealmInstancia.getInstancia(context).where(Analise::class.java).max("id").toInt() + 1
            } catch (e: ArrayIndexOutOfBoundsException) {
                0
            } catch (ex: NullPointerException) {
                0
            }
        }

        fun salvarAnalise(realm: Realm, dispositivo: Dispositivo) {
            val format = SimpleDateFormat("dd/MM/yyyy")
            val dt = format.format(Date())
            val dataAtual: Date = format.parse(dt)
            val analiseRealm = realm.where(Analise::class.java).equalTo("date", dataAtual).equalTo("dispositovo.id", dispositivo.id).findFirst()
            val tarifa: Tarifa?

            if (analiseRealm == null) {
                val analise = Analise()
                analise.id = try {
                    realm.where(Analise::class.java).max("id").toInt() + 1
                } catch (e: ArrayIndexOutOfBoundsException) {
                    0
                } catch (ex: NullPointerException) {
                    0
                }
                analise.date = dataAtual
                analise.dispositovo = dispositivo

                tarifa = TarifaRepository.findLastTarifa(realm)
                var valorGastoDoDia: Double? = 0.0
                if (tarifa != null)
                    valorGastoDoDia = dispositivo.watts?.let { Calculo.calcularConsumo(it, tarifa?.valor!!, dispositivo.time!!) }
                analise.valor = valorGastoDoDia
                realm.copyToRealmOrUpdate(analise)
            } else {
                analiseRealm.dispositovo = dispositivo
                tarifa = TarifaRepository.findLastTarifa(realm)
                var valorGastoDoDia: Double? = 0.0
                if (tarifa != null)
                    valorGastoDoDia = dispositivo.watts?.let { Calculo.calcularConsumo(it, tarifa?.valor!!, dispositivo.time!!) }
                if (analiseRealm.valor == null || analiseRealm.valor!! < valorGastoDoDia!!)
                    analiseRealm.valor = valorGastoDoDia
                realm.copyToRealmOrUpdate(analiseRealm)
            }

        }

        fun getAnaliseByDataInicioEdataFim(dataFim: Date, dataInicio: Date, context: Context) = RealmInstancia.getInstancia(context)
                .where(Analise::class.java)
                .greaterThanOrEqualTo("date", dataInicio)
                .lessThanOrEqualTo("date", dataFim)
                .findAll()

        fun getAnaliseByData(data: Date, context: Context) = RealmInstancia.getInstancia(context)
                .where(Analise::class.java)
                .equalTo("date", data)
                .findFirst()

    }
}