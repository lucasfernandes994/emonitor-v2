package com.example.ferna_xj0mjha.emonitor.view.fragment

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.repository.ConsumoRepository
import com.example.ferna_xj0mjha.emonitor.view.adapter.ConsumoAdpter

/**
 * Created by ferna_xj0mjha on 23/09/2017.
 */
class AnaliseInfoFragment : BaseFragment() {
    var rcConsumo: RecyclerView? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_analise_info, container, false)
        initComponents(view!!)
        setAdpter()
        return view
    }

    private fun initComponents(view: View) {
        rcConsumo = view.findViewById(R.id.rc_consumo)
    }

    private fun setAdpter() {
        val consumos = ConsumoRepository().selectAllConsumoFromRealm(context)
        val adapter = ConsumoAdpter(consumos, true, context)
        rcConsumo?.adapter = adapter
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        rcConsumo?.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        rcConsumo?.layoutManager = layoutManager
    }
}