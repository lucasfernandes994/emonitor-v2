package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context

/**
 * Created by ferna_xj0mjha on 02/09/2017.
 */
interface ILogout {

    interface IModel {
        fun logout(presenter : IPresenter)
    }

    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context
        fun logout()
        fun logoutError()
        fun logoutSucess()
    }

    interface IView {
        fun logoutError()
        fun logoutSucess()
    }
}