package com.example.ferna_xj0mjha.emonitor.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IConsumoRepository
import com.example.ferna_xj0mjha.emonitor.presenter.ConsumoRepositoryPresenter
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import java.text.SimpleDateFormat

/**
 * Created by ferna_xj0mjha on 07/10/2017.
 */
class ConsumoAdpter(data: OrderedRealmCollection<Consumo>?, autoUpdate: Boolean, context: Context) : RealmRecyclerViewAdapter<Consumo, ConsumoAdpter.ConsumoViewHolder>(data, autoUpdate), IConsumoRepository.IView {
    private var context: Context? = null
    private var presenter: IConsumoRepository.IPresenter? = null

    init {
        this.context = context
        this.presenter = ConsumoRepositoryPresenter(context)
        presenter?.setView(this)
    }

    override fun onBindViewHolder(holder: ConsumoViewHolder?, position: Int) {
        holder?.tvValor?.text = getItem(position)?.valorKwh?.toString()
        val format = SimpleDateFormat("dd/MMM/yyyy hh:mm:ss Z")
        format.format(getItem(position)?.date)
        holder?.tvData?.text = format.format(getItem(position)?.date).toString()
        holder?.btDelete?.setOnClickListener {
            presenter?.deleteConsumo(this.getItem(position)!!)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ConsumoViewHolder =
            ConsumoViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_consumo, parent, false))

    class ConsumoViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var tvData: TextView? = null
        var tvValor: TextView? = null
        var btDelete: ImageButton? = null

        init {
            tvData = itemView?.findViewById(R.id.tv_data)
            tvValor = itemView?.findViewById(R.id.tv_valor)
            btDelete = itemView?.findViewById(R.id.ib_delete)
        }
    }

    override fun sucessDelete() {
        notifyDataSetChanged()
    }

    override fun errorDelete() {
        Toast.makeText(context, "Não foi possível deletar.", Toast.LENGTH_SHORT).show()
    }
}