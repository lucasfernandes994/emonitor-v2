package com.example.ferna_xj0mjha.emonitor.model.repository

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.entity.Tarifa
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ITarifa
import io.realm.Realm

/**
 * Created by ferna_xj0mjha on 01/11/2017.
 */
class TarifaRepository : ITarifa.IModel {
    var presenter: ITarifa.IPresenter? = null

    companion object {
        fun findLastTarifa(context: Context): Tarifa =
                RealmInstancia.getInstancia(context).where(Tarifa::class.java).findAll().last()

        fun findLastTarifa(realm: Realm): Tarifa =
                realm.where(Tarifa::class.java).findAll().last()

    }

    constructor(presenter: ITarifa.IPresenter) {
        this.presenter = presenter
    }

    override fun save(tarifa: Tarifa) {
        presenter?.getContext()?.let {
            RealmInstancia.getInstancia(it).executeTransaction { realm ->
                realm.insertOrUpdate(tarifa)
            }
            RealmInstancia.getInstancia(it).close()
            presenter?.succesSave()
            presenter?.setTarifa(tarifa.valor!!)
        }
    }

    override fun deleteAll() {
        presenter?.getContext()?.let {
            RealmInstancia.getInstancia(it).executeTransaction { realm ->
                realm?.where(Tarifa::class.java)?.findAll()?.deleteAllFromRealm()
            }
        }
    }

    override fun selectLast() {
        RealmInstancia.getInstancia(presenter?.getContext()!!).executeTransaction { realm ->
            val tarifa = realm?.where(Tarifa::class.java)?.findAll()
            try {
                if (tarifa != null)
                    presenter?.setTarifa(tarifa.last()!!.valor!!)
            } catch (e: java.lang.IndexOutOfBoundsException) {
                e.printStackTrace()
            } catch (ex: NullPointerException) {
                ex.printStackTrace()
            }
            realm.close()
        }
    }

    override fun nextId(): Int {
        return try {
            RealmInstancia.getInstancia(presenter?.getContext()!!).where(Modulo::class.java).max("id").toInt() + 1
        } catch (e: ArrayIndexOutOfBoundsException) {
            0
        } catch (ex: NullPointerException) {
            0
        }
    }
}