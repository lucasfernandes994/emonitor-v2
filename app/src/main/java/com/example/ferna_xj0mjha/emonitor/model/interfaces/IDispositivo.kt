package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import java.util.ArrayList


/**
 * Created by lucas on 22/07/17.
 */

interface IDispositivo {
    interface IPresenter {
        fun setView(view: IDispositivo.IView)
        fun getContext() : Context

        fun getExpenditure(modulo: Modulo)
        fun showExpenditure(dispositivo: ArrayList<Dispositivo>)
    }

    interface IModel {
        fun consultExpenditure(modulo: Modulo)
    }

    interface IView {
        fun showExpenditure(dispositivo: ArrayList<Dispositivo>)
        fun setAdapter()
    }
}
