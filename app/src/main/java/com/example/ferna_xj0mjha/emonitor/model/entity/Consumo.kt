package com.example.ferna_xj0mjha.emonitor.model.entity

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

/**
 * Created by lucas on 10/08/2017.
 */
open class Consumo : RealmObject() {
    @PrimaryKey
    var id: Int? = null
    var date: Date? = null
    var dataInicio: Date? = null
    var dataFim: Date? = null
    var valorKwh: Double? = null
    var dispositivos: RealmList<Dispositivo>? = null
}