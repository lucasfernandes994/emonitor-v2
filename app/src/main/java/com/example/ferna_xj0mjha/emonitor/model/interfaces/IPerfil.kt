package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback

/**
 * Created by ferna_xj0mjha on 19/09/2017.
 */

interface IPerfil {
    interface IPresenter {
        fun setView(view: IView)
        fun getContext(): Context

        fun atulizarPerfil(nome: String, email: String, senha: String, confirmSenha: String)

        fun atualizacaoBemsucedida()
        fun atualizacaoComErro(mensagem: String)
    }

    interface IModel {
        fun atulizarPerfil(usuario: Usuario, callback: ICallback)
    }

    interface IView {
        fun atualizacaoBemsucedida()
        fun atualizacaoComErro(mensagem: String)
        fun showProgess()
        fun hideProgress()
    }
}
