package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import com.example.ferna_xj0mjha.emonitor.infra.Constants
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.ModuloRepository
import com.example.ferna_xj0mjha.emonitor.model.repository.RealmInstancia
import io.realm.RealmResults


/**
 * Created by lucas on 19/08/2017.
 */

class ModuloRepositoryPresenter(context: Context) : IModuloRepository.IPresenter {
    override fun findModuloById(id: Int, context: Context): Modulo = RealmInstancia.getInstancia(context)
            .where(Modulo::class.java)
            .equalTo("id", id)
            .findFirst()

    private var view: IModuloRepository.IView? = null
    private var context: Context? = null
    private var model: IModuloRepository.IModel? = null

    override fun setView(view: IModuloRepository.IView) {
        this.view = view
    }

    init {
        this.context = context
        this.model = ModuloRepository(this.context!!, this)
    }

    override fun findAll(context: Context): RealmResults<Modulo> = model?.findAll(context)!!

    override fun saveModulo(modlulo: Modulo) {
        if (modlulo.dispositivo?.nome != null && modlulo.dispositivo?.nome?.replace(" ", "") != "") {
            val modulo: Modulo? = RealmInstancia.getInstancia(context!!).where(Modulo::class.java).equalTo("ip", modlulo.ip).findFirst()
            if (modulo == null) {
                context?.let { model?.save(modlulo, it) }
                val intent = Intent(Constants.BroadcastReceiver.REFRESH_MODULO_ADAPTER)
                LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
            } else
                errorSameIp()
        } else
            errorCreateUpdate()
    }

    override fun upadateModulo(modulo: Modulo) {
        if (modulo.dispositivo?.nome != null && modulo.dispositivo?.nome?.replace(" ", "") != "") {
            val moduloRealm: Modulo? = RealmInstancia.getInstancia(context!!).where(Modulo::class.java).equalTo("ip", modulo.ip).findFirst()
            if (moduloRealm == null || moduloRealm.id == modulo.id) {
                context?.let {
                    model?.update(modulo, it)
                    val intent = Intent(Constants.BroadcastReceiver.REFRESH_MODULO_ADAPTER)
                    LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
                }
            } else
                errorSameIp()
        } else
            errorCreateUpdate()
    }

    override fun delete(modlulo: Modulo) {
        context?.let { model?.delete(modlulo, it) }
    }

    override fun sucessSave() {
        view?.sucessSave()
    }

    override fun errorDelete() {
        view?.errorDelete()
    }

    override fun sucessDelete() {
        view?.sucessDelete()
    }

    override fun errorCreateUpdate() {
        view?.errorCreateUpdate()
    }

    override fun errorSameIp() {
        view?.errorSameIp()
    }

    override fun sucessUpadate() {
        view?.sucessUpadate()
    }
}