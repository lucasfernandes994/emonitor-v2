package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback

/**
 * Created by ferna_xj0mjha on 26/08/2017.
 */
interface ISingUp {

    interface IPresenter {
        fun setView(view: IView)
        fun getContext() : Context

        fun createAccount(nome : String, email: String, senha: String)

    }

    interface IModel {
        fun createAccount(usuario: Usuario, callback : ICallback)
    }

    interface IView {
        fun accountSucess()
        fun accountError(erro : String)
        fun errorEmail(erro : String)
        fun hideProgress()
        fun showProgress()
    }
}