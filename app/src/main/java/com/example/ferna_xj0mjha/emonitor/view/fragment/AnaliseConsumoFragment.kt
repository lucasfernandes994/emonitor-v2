package com.example.ferna_xj0mjha.emonitor.view.fragment

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IAnalise
import com.example.ferna_xj0mjha.emonitor.presenter.AnaliseConsumoPresenter
import com.example.ferna_xj0mjha.emonitor.view.adapter.AnaliseAdapter
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData


/**
 * Created by ferna_xj0mjha on 23/09/2017.
 */
class AnaliseConsumoFragment : BaseFragment(), IAnalise.IView {
    var consumoPieChart: PieChart? = null
    var presenter: IAnalise.IPresenter? = null
    var rcAnalise: RecyclerView? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater?.inflate(R.layout.fragment_analise_consumo, container, false)
        initComponents(view!!)
        setPresenter()
        return view
    }

    fun initComponents(view: View) {
        this.consumoPieChart = view?.findViewById(R.id.consumo_pie_chart)
        this.rcAnalise = view.findViewById(R.id.rc_analise)
    }

    override fun showAnalysis(pieData: PieData, consumo: Consumo) {
        consumoPieChart?.animateY(1000)
        consumoPieChart?.description?.text = "Valor gasto em R$"
        consumoPieChart?.saveToGallery("/sd/mychart.jpg", 85)
        consumoPieChart?.data = pieData

        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rcAnalise?.layoutManager = layoutManager
        val analiseAdapter = AnaliseAdapter(consumo.dispositivos, true, consumo, context)
        rcAnalise?.adapter = analiseAdapter

        val dividerItemDecoration = DividerItemDecoration(rcAnalise?.getContext(), layoutManager.orientation)
        rcAnalise?.addItemDecoration(dividerItemDecoration)

    }

    override fun reportError(mensagem: String) {
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show()
    }

    private fun setPresenter() {
        this.presenter = AnaliseConsumoPresenter(context)
        this.presenter?.setView(this)

    }

    override fun makeAnalysis() {
        this.presenter?.makeAnalysis()
    }
}