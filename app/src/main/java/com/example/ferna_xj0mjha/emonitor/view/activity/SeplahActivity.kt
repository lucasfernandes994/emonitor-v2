package com.example.ferna_xj0mjha.emonitor.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.widget.ProgressBar
import com.example.ferna_xj0mjha.emonitor.R

class SeplahActivity : BaseActivity() {
    private val SPLASH_TIME_OUT: Long = 3000
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed({
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }, SPLASH_TIME_OUT)

        var progresBar = findViewById<ProgressBar>(R.id.progressBar2)
    }
}
