package com.example.ferna_xj0mjha.emonitor.model.interfaces

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.model.entity.Consumo
import com.example.ferna_xj0mjha.emonitor.model.entity.Dispositivo
import java.util.*

/**
 * Created by ferna_xj0mjha on 11/08/2017.
 */
interface IConsumo {
    interface IPresenter {
        fun setView(view: IConsumo.IView)
        fun getContext(): Context

        fun createNew()
        fun selectLast()
        fun setConsumo(consumo: Consumo)
        fun genereteMultipleReport(dataInicio: Date, dataFim: Date, valorKwh: Double)
        fun deleteConsumo(consumo: Consumo)
        fun susccesDelete()
        fun errorDelete(mensagem: String)

        fun showReport(boolean: Boolean)
    }

    interface IModel {
        fun selectLast()
        fun createNew()
        fun genereteMultipleReport(consumo: Consumo, dataInicio: Date, dataFim: Date)
        fun deleteConsumo(consumo: Consumo)
    }

    interface IView {
        fun setConsumo(consumo: Consumo)
        fun showReport()
        fun showReportMessage(message: String)
        fun susccesDelete()
        fun errorDelete(mensagem: String)
    }
}