package com.example.ferna_xj0mjha.emonitor.presenter

import android.content.Context
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Usuario
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ISingUp
import com.example.ferna_xj0mjha.emonitor.model.service.calllback.ICallback
import com.example.ferna_xj0mjha.emonitor.model.service.SingnUpServices

class SingnUpPresenter(context: Context) : ISingUp.IPresenter {
    private var view: ISingUp.IView? = null
    private var context: Context? = null
    private var model: ISingUp.IModel? = null

    override fun setView(view: ISingUp.IView) {
        this.view = view
    }

    init {
        this.context = context
        this.model = SingnUpServices(this, context)
    }

    override fun getContext(): Context {
        val context1 = this.context
        if (context1 != null) return context1 else return throw NullPointerException("Expression 'this!!.context' must not be null")
    }

    override fun createAccount(nome: String, email: String, senha: String) {
        if (!nome.isEmpty() && !email.isEmpty() && !senha.isEmpty()) {
            if (!email.contains('@', true)) {
                view?.accountError(context?.resources?.getString(R.string.email_invalido)!!)
                return
            }
            val usuario = Usuario()
            usuario.email = email
            usuario.nome = nome
            usuario.senha = senha
            view?.showProgress()
            model?.createAccount(usuario, object : ICallback {
                override fun callbackError(mensagem: String) {
                    view?.hideProgress()
                    view?.accountError("Não foi possível fazer uma nova conta.")
                }

                override fun callbackSucces() {
                    view?.hideProgress()
                    view?.accountSucess()
                }
            })
        } else
            view?.accountError("Insira tosdos os campos.")
    }
}