package com.example.ferna_xj0mjha.emonitor.view.adapter

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.support.v7.app.AlertDialog
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.entity.Calculo
import com.example.ferna_xj0mjha.emonitor.model.entity.Modulo
import com.example.ferna_xj0mjha.emonitor.model.interfaces.IModuloRepository
import com.example.ferna_xj0mjha.emonitor.presenter.ModuloRepositoryPresenter
import io.realm.RealmRecyclerViewAdapter
import io.realm.RealmResults


/**
 * Created by lucas on 19/08/2017.
 */
class ModuloAdapter(private val modulos: RealmResults<Modulo>?, autoUpdate: Boolean, context: Context, clickListner: View.OnClickListener) : RealmRecyclerViewAdapter<Modulo,
        ModuloAdapter.DispositivoViewHolder>(modulos, autoUpdate), IModuloRepository.IView {

    var presenter: IModuloRepository.IPresenter? = null
    var context: Context? = null
    var clickListner: View.OnClickListener? = null

    init {
        this.context = context
        this.presenter = ModuloRepositoryPresenter(context)
        presenter?.setView(this)
        this.clickListner = clickListner
    }

    override fun onBindViewHolder(holder: DispositivoViewHolder?, position: Int) {
        val modulo: Modulo? = getItem(position)
        holder?.nome?.text = modulo?.dispositivo?.nome
        if (modulo?.status != null)
            if (modulo.status.equals("on"))
                holder?.valor?.text = "Ligado"
            else
                holder?.valor?.text = "Desligado"

        if (modulo?.dispositivo?.time != null)
            holder?.consumo?.text = Calculo.millisToTime(modulo.dispositivo?.time!!)

        holder?.bt_delete?.setOnClickListener {
            val builder = AlertDialog.Builder(this.context!!)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setTitle(context?.getString(R.string.dispositivo))
                    .setMessage(context?.resources?.getString(R.string.desej_excluir_este_dispositivo))
                    .setPositiveButton(context?.getString(R.string.sim), { dialog, which ->
                        modulo?.let { it1 -> presenter?.delete(it1) }
                    })
                    .setNegativeButton(context?.getString(R.string.nao), null)
                    .show()
            val nbutton = builder.getButton(DialogInterface.BUTTON_NEGATIVE)
            nbutton.setBackgroundColor(Color.WHITE)
            context?.resources?.getColor(R.color.colorPrimary)?.let { it1 -> nbutton.setTextColor(it1) }
            val pbutton = builder.getButton(DialogInterface.BUTTON_POSITIVE)
            pbutton.setBackgroundColor(Color.WHITE)
            context?.resources?.getColor(R.color.colorPrimary)?.let { it1 -> pbutton.setTextColor(it1) }
            val pneutral = builder.getButton(DialogInterface.BUTTON_NEUTRAL)
            pneutral.setBackgroundColor(Color.WHITE)
            context?.resources?.getColor(R.color.colorPrimary)?.let { it1 -> pneutral.setTextColor(it1) }
        }
        holder?.itemView?.setOnClickListener(clickListner)
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): DispositivoViewHolder =
            DispositivoViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.item_modulo, parent, false))

    override fun getItemCount(): Int = (if (modulos != null) modulos.size else null)!!

    override fun errorDelete() {
        Toast.makeText(context, "Não foi possível deletar.", Toast.LENGTH_SHORT).show()
    }

    override fun errorCreateUpdate() {
        Toast.makeText(context, "Erro au criar.", Toast.LENGTH_SHORT).show()
    }

    override fun errorSameIp() {
        Toast.makeText(context, "Ip indisponível.", Toast.LENGTH_SHORT).show()
    }

    override fun sucessSave() {
        Toast.makeText(context, "Disposivo cadastrado.", Toast.LENGTH_SHORT).show()
    }

    override fun sucessDelete() {
        Toast.makeText(context, "Dispositivo deletado com sucesso.", Toast.LENGTH_SHORT).show()
    }

    class DispositivoViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        var nome: TextView? = null
        var valor: TextView? = null
        var consumo: TextView? = null
        var bt_delete: ImageButton? = null

        init {
            this.nome = itemView?.findViewById(R.id.tv_nome)
            this.valor = itemView?.findViewById(R.id.tv_valor)
            this.consumo = itemView?.findViewById(R.id.tv_watts)
            this.bt_delete = itemView?.findViewById(R.id.ib_delete)
        }
    }

    override fun sucessUpadate() {
        Toast.makeText(context!!, "Atualização feita.", Toast.LENGTH_SHORT).show()
    }
}