package com.example.ferna_xj0mjha.emonitor.infra

/**
 * Created by lucas on 26/07/17.
 */

class Constants {
    object Preferences {
        val SHARED_PREFERENCE = "shared_preference"
        val USER_ACTIVE = "active"
    }

    object UserData {
        val USER_DATABASE = "user_preference"
    }

    object Url{
        //val LOGIN_URL = "http://192.168.43.103/EmonitorApi/"
        val LOGIN_URL = "http://192.168.0.11/EmonitorApi/"
    }

    object ParametroModulo{
        val WATTS = "watts"
        val TIME = "time"
        val TURN_OFF = "off"
        val TURN_ON = "on"
        val RELAY_STATUS = "relay_state"
        val GET_MODULO = "get_modulo"
    }

    object BroadcastReceiver{
        val REFRESH_MODULO_ADAPTER = "refresh_modulo_adapter"
        val ANALISYS_RECEIVER = "analisys_receiver"
    }

    object IntentParam{
        val ID_MODULO = "id_modulo"
    }
}
