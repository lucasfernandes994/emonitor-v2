package com.example.ferna_xj0mjha.emonitor.view.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.example.ferna_xj0mjha.emonitor.R
import com.example.ferna_xj0mjha.emonitor.model.interfaces.ILogin
import com.example.ferna_xj0mjha.emonitor.presenter.LoginPresenter


class LoginActivity : BaseActivity(), ILogin.IView {
    private var progress: ProgressBar? = null
    private var presenter: ILogin.IPresenter? = null
    private var btLogin: Button? = null
    private var btSignup: Button? = null
    private var etEmail: EditText? = null
    private var etSenha: EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initPresenter()
        initComponents()
        initHandlers()
    }

    override fun loginSucess() {
        val i = Intent(applicationContext, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
        finish()
    }

    override fun loginError(mensagem: String) {
        hideProgress()
        Toast.makeText(applicationContext, mensagem, Toast.LENGTH_SHORT).show()
    }

    override fun hideProgress() {
        progress?.visibility = View.GONE
        btLogin?.visibility = View.VISIBLE
        btSignup?.visibility = View.VISIBLE
        etEmail?.visibility = View.VISIBLE
        etSenha?.visibility = View.VISIBLE
    }

    override fun showProgress() {
        progress?.visibility = View.VISIBLE
        btLogin?.visibility = View.GONE
        btSignup?.visibility = View.GONE
        etEmail?.visibility = View.GONE
        etSenha?.visibility = View.GONE
    }

    @SuppressLint("ResourceAsColor")
    private fun initComponents() {
        btSignup = findViewById<Button>(R.id.bt_signup)
        btLogin = findViewById<Button>(R.id.bt_login)
        etEmail = findViewById<EditText>(R.id.et_email)
        etSenha = findViewById(R.id.et_senha)
        progress = findViewById(R.id.progressBar)
        progress?.progressDrawable?.setColorFilter(R.color.colorPrimary, android.graphics.PorterDuff.Mode.SRC_IN)
    }

    private fun initPresenter() {
        this.presenter = LoginPresenter(applicationContext)
        this.presenter!!.setView(this)
        presenter?.userLogged()
    }

    private fun initHandlers() {
        btSignup?.setOnClickListener {
            startActivity(Intent(this, SingnUpActivity::class.java))
        }
        btLogin?.setOnClickListener {
            presenter!!.validCredential(etEmail!!.text.toString(), etSenha!!.text.toString())
        }
    }

}

